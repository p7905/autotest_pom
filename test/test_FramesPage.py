import pytest

from datas.links_page import LinksPage
from datas.data_example import Frame
from methods.AlertsPage import AlertsPage
from locators.AlertsPage_locators import FramesAlertsPageLocators

class TestFramesAlertsPage():
    '''
    ## Часть 5: Frames
    ### Кейс №26: Страница в странице
    1. Открыть категорию Alerts, Frame & Windows, перейти в Frames
    2. Определить текст, который находится в большом фрейме (id="frame1")
    3. Проверить полученный текст на соответствие «This is a sample page»
    '''
    def test_page_in_page(self, browser):
        # 1
        frames_page = AlertsPage(browser, LinksPage.LINK_ALERTS)
        frames_page.open()

        frames_page.go_to_and_click(*FramesAlertsPageLocators.FRAMES)

        # 2
        frame = frames_page.browser.find_element(*FramesAlertsPageLocators.FRAME_MAX)
        frames_page.browser.switch_to.frame(frame)
        text_frame = frames_page.browser.find_element(*FramesAlertsPageLocators.FRAME_TEXT).text

        # 3
        assert text_frame == 'This is a sample page', f"Во фрейме текст {text_frame} вместо 'This is a sample page'"

    '''
    ### Кейс №27: Маленькая страница в странице
    1. Открыть категорию Alerts, Frame & Windows, перейти в Frames
    2. Изменить координат в маленьком фрейме (id="frame2") на 1000 по осям X и Y
    '''
    def test_minipage_in_page(self, browser):
        # 1
        frames_page = AlertsPage(browser, LinksPage.LINK_ALERTS)
        frames_page.open()

        frames_page.go_to_and_click(*FramesAlertsPageLocators.FRAMES)

        # 2
        frame = frames_page.browser.find_element(*FramesAlertsPageLocators.FRAME_MINI)

        # 3
        frames_page.set_attribut(frame, 'width', Frame.size)
        frames_page.set_attribut(frame, 'height', Frame.size)

        frame_width = frame.get_attribute("width")
        frame_height = frame.get_attribute("height")

        assert frame_width == Frame.size, f"Размер {frame_width} вместо {Frame.size}"
        assert frame_height == Frame.size, f"Размер {frame_height} вместо {Frame.size}"