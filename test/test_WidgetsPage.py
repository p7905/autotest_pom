import pytest

import copy
import time
import datetime

from datas.links_page import LinksPage
from methods.WidgetsPage import WidgetsPage
from datas.data_example import AutoComplite
from datas.data_example import DatePicker
from datas.data_example import Slider

from locators.WidgetsPage_locators import *


accordian = [[AccordianWidgetsPageLocators.ACCORDIAN_CARD1, AccordianWidgetsPageLocators.ACCORDIAN_CARD1_TEXT],
            [AccordianWidgetsPageLocators.ACCORDIAN_CARD2, AccordianWidgetsPageLocators.ACCORDIAN_CARD2_TEXT],
            [AccordianWidgetsPageLocators.ACCORDIAN_CARD3, AccordianWidgetsPageLocators.ACCORDIAN_CARD3_TEXT]]

##@pytest.mark.skip
class TestAccordiansWidgetsPage():
    '''
    ## Часть 6: Widgets
    ### Кейс №28: Гармошка
    1. Открыть категорию Widgets, перейти в Accordian
    2. Поочередно открывать каждый блок в гармошке и проверять наличие нужного текста и отсутствие ненужного

    > p.s. в 1 блоке не должно быть текста 2 и 3, во 2 нет текста 1 и 3, в 3 нет текста 1 и 2
    '''

    def test_accordian(self, browser):
        card_text = []

        # 1
        widgets_page = WidgetsPage(browser, LinksPage.LINK_WIDGETS)
        widgets_page.open()

        widgets_page.go_to_and_click(*AccordianWidgetsPageLocators.ACCORDIAN)

        # 2
        for i in range(len(accordian)):
            assert widgets_page.element_is_visible(*accordian[i][0]), f"Элемент гармошки {i + 1} отсутствует"

            card = widgets_page.browser.find_element(*accordian[i][0])
            if i > 0:
                widgets_page.go_to_element(card)
                card.click()

            card_text.append(' '.join([elem.text for elem in widgets_page.browser.find_elements(*accordian[i][1])]))

        for i in range(len(card_text)):
            copy_cards = copy.deepcopy(card_text)
            copy_cards.pop(i)
            assert card_text[i] not in copy_cards, f"Ожидалось что текст {i + 1} карточки не будет присутствовать в других карточках"

class TestAutoCompliteWidgetsPage():
    '''
    ### Кейс №29: Автокомплит нескольких слов
    1. Открыть категорию Widgets, перейти в Auto Complite
    2. В поле «Type multiple color names» поочередно ввести и выбрать следующее (не удаляя предыдущие):
    	2.1 «re» и выбрать red
    	2.2 «r» и выбрать purple
    	2.3 «q» и выбрать aqua
    3. Проверить слова и их последовательность в поле
    '''
##    @pytest.mark.skip
    def test_auto_complete_multiple(self, browser):
        color_select = ""

        # 1
        widgets_page = WidgetsPage(browser, LinksPage.LINK_WIDGETS)
        widgets_page.open()

        widgets_page.go_to_and_click(*AutoCompliteWidgetsPageLocators.AUTO_COMPLIT)

        # 2

        input_color = widgets_page.browser.find_element(*AutoCompliteWidgetsPageLocators.AUTO_COMPLIT_INPUT_MULT)

        for i in range(len(AutoComplite.color_full_mult)):
            widgets_page.select_full_color_from_part_name(input_color, AutoComplite.color_full_mult[i], *AutoCompliteWidgetsPageLocators.AUTO_COMPLIT_SELECT)

        color_select = ' '.join([elem.text.upper() for elem in widgets_page.browser.find_elements(*AutoCompliteWidgetsPageLocators.AUTO_COMPLIT_MULT_TEXT)])
        assert (' '.join([elem.upper() for elem in AutoComplite.color_full_mult])) == color_select

    '''
    ### Кейс №30: Автокомплит одного слова
    1. Открыть категорию Widgets, перейти в Auto Complite
    2. В поле «Type single color name» ввести «b» и выбрать blue
    3. В это же поле ввести «d» и выбрать indigo
    4. Проверить поле на наличие последнего слова и отсутствие первого
    '''
##    @pytest.mark.skip
    def test_auto_complete_single(self, browser):
        color_select = ""

        # 1
        widgets_page = WidgetsPage(browser, LinksPage.LINK_WIDGETS)
        widgets_page.open()

        widgets_page.go_to_and_click(*AutoCompliteWidgetsPageLocators.AUTO_COMPLIT)

        # 2
        input_color = widgets_page.browser.find_element(*AutoCompliteWidgetsPageLocators.AUTO_COMPLIT_INPUT_SING)

        for i in range(len(AutoComplite.color_full_sing)):
            widgets_page.select_full_color_from_part_name(input_color, AutoComplite.color_full_sing[i], *AutoCompliteWidgetsPageLocators.AUTO_COMPLIT_SELECT)

        color_select = widgets_page.browser.find_element(*AutoCompliteWidgetsPageLocators.AUTO_COMPLIT_SING_TEXT).text.upper()

        assert AutoComplite.color_full_sing[0].upper() != color_select, f"Вместо '{color_full[0]}' должно быть '{color_full[1]}'"
        assert AutoComplite.color_full_sing[1].upper() == color_select, f"Вместо '{color_select}' должно быть '{color_full[1]}'"

class TestDatePickerWidgetsPage():
    '''
    ### Кейс №31: Дата и время
    1. Открыть категорию Widgets, перейти в Date Picker
    2. В поле «Date And Time» выбрать дату: 27 ноября 1940 года 12:00
    3. Закрыть блок выбора даты и времени
    4. Проверить данные в поле на соответствие введенным данным
    '''
##    @pytest.mark.skip
    def test_data_picker(self, browser):
        # 1
        widgets_page = WidgetsPage(browser, LinksPage.LINK_WIDGETS)
        widgets_page.open()

        widgets_page.go_to_and_click(*DatePickerWidgetsPageLocators.DATE_PICKER)

        # 2
        date_time_input = widgets_page.browser.find_element(*DatePickerWidgetsPageLocators.DATE_AND_TIME_INPUT)
        date_time_input.click()
        assert widgets_page.element_is_visible(*DatePickerWidgetsPageLocators.DATE_PICKER_BLOC) is True, "Блок выбора даты и времени не появился"

        widgets_page.browser.find_element(*DatePickerWidgetsPageLocators.MONTH_BLOC).click()
        widgets_page.select_month(DatePicker.date_time.strftime('%B'), date_time_input.get_attribute("value"), \
                    DatePickerWidgetsPageLocators.MONTH_SELECTED, DatePickerWidgetsPageLocators.MONTH_SELECT)

        widgets_page.browser.find_element(*DatePickerWidgetsPageLocators.YEAR_BLOC).click()
        widgets_page.select_year(DatePicker.date_time.year, date_time_input.get_attribute("value"), \
        DatePickerWidgetsPageLocators.YEAR_SELECTED, DatePickerWidgetsPageLocators.YEAR_SELECT, \
        DatePickerWidgetsPageLocators.YEAR_NAVIGATION_PREV)

        widgets_page.select_date(DatePicker.date_time.day, date_time_input.get_attribute("value"), DatePickerWidgetsPageLocators.DATE_SELECT)

        widgets_page.select_time(DatePicker.date_time.strftime('%H:%M'), date_time_input.get_attribute("value"), DatePickerWidgetsPageLocators.TIME_SELECT)

        # 3
        # блок закрывается сам после выбора времени, проверка что блок закрыт
        assert widgets_page.element_is_visible(*DatePickerWidgetsPageLocators.DATE_PICKER_BLOC) is False, "Блок выбора даты и времени не закрылся"

        # 4
        date_res = date_time_input.get_attribute("value")
    #    date_inp = date_time.strftime('%B %#d, %Y %I:%M %p') # Win - '#'
    #    date_inp = date_time.strftime('%B %-d, %Y %I:%M %p')  # Linux - '-'
        date_inp = DatePicker.date_time.strftime('%B %e, %Y %I:%M %p') # работает везде - 'e'
        assert date_res == date_inp, f"Должна быть дата {date_res} вместо {date_inp}"

class TestSliderWidgetsPage():
    '''
    ### Кейс №32: Слайдер
    1. Открыть категорию Widgets, перейти в Slider
    2. Установить значение в слайдере на 72
    3. Проверить установленное значение
    '''
##    @pytest.mark.skip
    def test_slider(self, browser):
        # 1
        widgets_page = WidgetsPage(browser, LinksPage.LINK_WIDGETS)
        widgets_page.open()

        widgets_page.go_to_and_click(*SliderWidgetsPageLocators.SLIDER)

        # 2
        slider = widgets_page.browser.find_element(*SliderWidgetsPageLocators.SLIDER_INPUT)
        sdvig = (Slider.value_slider - 50) * (slider.size['width'] - 20) / int(slider.get_attribute("max"))

        time.sleep(1)
        widgets_page.select_value_on_slider(slider, sdvig)
        time.sleep(1)

        # 3
        val_new = widgets_page.browser.find_element(*SliderWidgetsPageLocators.SLIDER_VALUE).get_attribute("value")
        assert val_new == str(Slider.value_slider), f"Слайдер передвинут на {val_new} вместо {Slider.value_slider}"

class TestProgressBarWidgetsPage():
    '''
    ### Кейс №33: Индикатор - старт
    1. Открыть категорию Widgets, перейти в Progress Bar
    2. Нажать кнопку Start
    3. Проверить изменение индикатора
    '''
##    @pytest.mark.skip
    def test_progress_bar_start(self, browser):
        # 1
        widgets_page = WidgetsPage(browser, LinksPage.LINK_WIDGETS)
        widgets_page.open()

        widgets_page.go_to_and_click(*ProgressBarWidgetsPageLocators.PROGRESS_BAR)

        # 2
        button = widgets_page.browser.find_element(*ProgressBarWidgetsPageLocators.BUTTON_START_STOP)
        button.click()

        # 3
        assert widgets_page.element_is_visible(*ProgressBarWidgetsPageLocators.PROGRESS_INFO) is True, "Должен быть индикатор текущего процесса"
        assert widgets_page.element_is_visible(*ProgressBarWidgetsPageLocators.PROGRESS_SUCCESS) is False, "Индикатора окончания не должно бытья"
        assert button.text == 'Stop', f"Надпись {button.text} вместо 'Stop'"
        progress_bar = widgets_page.browser.find_element(*ProgressBarWidgetsPageLocators.PROGRESS_INFO)
        assert int(progress_bar.text.replace('%', '')) > 0, f"Процесс не запустился"

    '''
    ### Кейс №34: Индикатор - стоп
    1. Открыть категорию Widgets, перейти в Progress Bar
    2. Нажать кнопку Start
    3. Дождаться индикатор до состояния <100%
    4. Нажать кнопку Stop
    5. Проверить изменение индикатора
    '''
##    @pytest.mark.skip
    def test_progress_bar_stop(self, browser):
        # 1
        widgets_page = WidgetsPage(browser, LinksPage.LINK_WIDGETS)
        widgets_page.open()

        widgets_page.go_to_and_click(*ProgressBarWidgetsPageLocators.PROGRESS_BAR)

        # 2 Start
        button = widgets_page.browser.find_element(*ProgressBarWidgetsPageLocators.BUTTON_START_STOP)
        button.click()

        # 3
        time.sleep(2)
        progress_bar = widgets_page.browser.find_element(*ProgressBarWidgetsPageLocators.PROGRESS_INFO)

        # 4 Stop
        button.click()
        old_value = int(progress_bar.text.replace('%', ''))
        time.sleep(1)
        new_value = int(progress_bar.text.replace('%', ''))

        # 5
        assert widgets_page.element_is_visible(*ProgressBarWidgetsPageLocators.PROGRESS_INFO) is True, "Должен быть индикатор текущего процесса"
        assert widgets_page.element_is_visible(*ProgressBarWidgetsPageLocators.PROGRESS_SUCCESS) is False, "Индикатора окончания не должно быть"
        assert button.text == 'Start', f"Надпись {button.text} вместо 'Start'"
        assert old_value == new_value, f"Процесс должен остановиться"

    '''
    ### Кейс №35: Индикатор — перезагрузка
    1. Открыть категорию Widgets, перейти в Progress Bar
    2. Нажать кнопку Start
    3. Дождаться заполнения индикатора до 100%
    4. Нажать кнопку Reset
    5. Проверить изменение индикатора
    '''
##    @pytest.mark.skip
    def test_progress_bar_reset(self, browser):
        # 1
        widgets_page = WidgetsPage(browser, LinksPage.LINK_WIDGETS)
        widgets_page.open()

        widgets_page.go_to_and_click(*ProgressBarWidgetsPageLocators.PROGRESS_BAR)

        # 2 Start
        widgets_page.browser.find_element(*ProgressBarWidgetsPageLocators.BUTTON_START_STOP).click()

        # 3 100%
        while True:
            if widgets_page.element_is_visible(*ProgressBarWidgetsPageLocators.PROGRESS_SUCCESS, 2) is True:
                break

        value_prog_new = int(widgets_page.browser.find_element(*ProgressBarWidgetsPageLocators.PROGRESS_SUCCESS).text.replace('%', ''))

        assert widgets_page.element_is_visible(*ProgressBarWidgetsPageLocators.PROGRESS_INFO) is False, "Индикатора старта не должно быть"
        assert widgets_page.element_is_visible(*ProgressBarWidgetsPageLocators.BUTTON_RESET) is True, "Должна быть кнопка 'Reset'"
        assert widgets_page.element_is_visible(*ProgressBarWidgetsPageLocators.BUTTON_START_STOP) is False, "Кнопок 'Start/Stop' не должно быть"
        assert value_prog_new == 100, f"Процесс не закончился"

        # 4 Reset
        widgets_page.browser.find_element(*ProgressBarWidgetsPageLocators.BUTTON_RESET).click()

        # 5
        assert widgets_page.element_is_visible(*ProgressBarWidgetsPageLocators.PROGRESS_SUCCESS) is False, "Индикатора окончания не должно быть"
        assert widgets_page.element_is_present(*ProgressBarWidgetsPageLocators.PROGRESS_INFO) is True, "Должен быть индикатор старта"
        assert widgets_page.element_is_visible(*ProgressBarWidgetsPageLocators.BUTTON_RESET) is False, "Кнопки 'Reset' не должно быть"
        assert widgets_page.element_is_visible(*ProgressBarWidgetsPageLocators.BUTTON_START_STOP) is True, "Должна быть кнопка 'Start'"

        button = widgets_page.browser.find_element(*ProgressBarWidgetsPageLocators.BUTTON_START_STOP)
        value_prog_new = int(widgets_page.browser.find_element(*ProgressBarWidgetsPageLocators.PROGRESS_INFO).text.replace('%', '').replace('', '0'))
        assert button.text == 'Start', f"Надпись {button.text} вместо 'Start'"
        assert value_prog_new == 0, f"Процесс не перегрузился"


class TestTabsWidgetsPage():
    '''
    ### Кейс №36: Вкладки
    1. Открыть категорию Widgets, перейти в Tabs
    2. Поочередно открывать каждый блок вкладок и проверять наличие нужного текста и отсутствие ненужного

    > p.s. в 1 блоке не должно быть текста 2 и 3, во 2 нет текста 1 и 3, в 3 нет текста 1 и 2
    '''
##    @pytest.mark.skip
    def test_tabs(self, browser):
        tabs_text = []
        tabs = [TabsWidgetsPageLocators.WHAT_TEXT,
                TabsWidgetsPageLocators.ORIGIN_TEXT,
                TabsWidgetsPageLocators.USE_TEXT]

        # 1
        widgets_page = WidgetsPage(browser, LinksPage.LINK_WIDGETS)
        widgets_page.open()

        widgets_page.go_to_and_click(*TabsWidgetsPageLocators.TABS)

        # 2
        tabs_list = widgets_page.browser.find_elements(*TabsWidgetsPageLocators.NAV_TABS_LIST)

        for i in range(len(tabs_list)):
            tabs_list[i].click()
            tabs_text.append(' '.join([elem.text for elem in widgets_page.browser.find_elements(*tabs[i])]))

        for i in range(len(tabs_text)):
            copy_tabs = copy.deepcopy(tabs_text)
            copy_tabs.pop(i)
            assert tabs_text[i] not in copy_tabs, f"Ожидалось что текст {i + 1} вкладки не будет присутствовать в других вкладках"

class TestToolTipsWidgetsPage():
    '''
    ### Кейс №37: Наведение
    1. Открыть категорию Widgets, перейти в Tool Tips
    2. Навести мышку на слово «Contrary» из блока с текстом
    3. Проверить наличие текста «You hovered over the Contrary» в появившемся блоке под словом
    '''
##    @pytest.mark.skip
    def test_tool_tips(self, browser):
        templ_text = "You hovered over the Contrary"

        # 1
        widgets_page = WidgetsPage(browser, LinksPage.LINK_WIDGETS)
        widgets_page.open()

        widgets_page.go_to_and_click(*ToolTipsWidgetsPageLocators.TOOL_TIPS)

        # 2
        widgets_page.move_cursor_to_element(widgets_page.browser.find_element(*ToolTipsWidgetsPageLocators.TEXT_LINK))
        assert widgets_page.element_is_visible(*ToolTipsWidgetsPageLocators.TOOLTIP) is True, "Должна появится подсказка для элемента"

        # 3
        text_tooltip = widgets_page.browser.find_element(*ToolTipsWidgetsPageLocators.TOOLTIP).text
        assert text_tooltip == templ_text, f"Должен быть текст {templ_text} вместо {text_tooltip}"

class TestMenuTipsWidgetsPage():
    '''
    ### Кейс №38: Меню
    1. Открыть категорию Widgets, перейти в Menu
    2. Поочередно навести мышку на меню: «Main Item 2» => «SUB SUB LIST» => «Sub Sub Item 2»
    3. Проверить видимость объектов, с которыми происходили взаимодействия
    '''
##    @pytest.mark.skip
    def test_menu(self, browser):
        # 1
        widgets_page = WidgetsPage(browser, LinksPage.LINK_WIDGETS)
        widgets_page.open()

        widgets_page.go_to_and_click(*MenuWidgetsPageLocators.MENU)

        # 2 - 3
        main_item_2 = widgets_page.browser.find_element(*MenuWidgetsPageLocators.MAIN_ITEM_2)
        widgets_page.move_cursor_to_element(main_item_2)
        assert widgets_page.element_is_visible(*MenuWidgetsPageLocators.MAIN_ITEM_2) is True, f"Элемента меню {main_item_2.text} нет"

        sub_sub_list = widgets_page.browser.find_element(*MenuWidgetsPageLocators.SUB_SUB_LIST)
        widgets_page.move_cursor_to_element(sub_sub_list)
        assert widgets_page.element_is_visible(*MenuWidgetsPageLocators.SUB_SUB_LIST) is True, f"Элемента меню {sub_sub_list.text} нет"

        sub_sub_item_2 = widgets_page.browser.find_element(*MenuWidgetsPageLocators.SUB_SUB_ITEM_2)
        widgets_page.move_cursor_to_element(sub_sub_item_2)
        assert widgets_page.element_is_visible(*MenuWidgetsPageLocators.SUB_SUB_ITEM_2) is True, f"Элемента меню {sub_sub_item_2.text} нет"
