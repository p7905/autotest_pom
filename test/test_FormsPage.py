import pytest
import datetime

from datas.links_page import LinksPage
from datas.data_example import Forms
from methods.FormsPage import FormsPage

from locators.FormsPage_locators import *


class TestPracticeFormFormsPage():
    '''
    ## Часть 3: Forms
    ### Кейс №19: Валидные данные в форме
    1. Открыть категорию Forms, перейти в Practice Form
    2. Заполнить все поля валидными данными (файл загружать не обязательно)
    3. Проверить правильность введенных данных во всплывающем модальном окне
    '''
    ##@pytest.mark.skip
    def test_valid_data_form(self, browser):
        data_fill = dict()

        # 1
        forms_page = FormsPage(browser, LinksPage.LINK_FORMS)
        forms_page.open()

        forms_page.browser.find_element(*FormsPage_locators.PRACTICE_FORM).click()

        # 2
    ##    remove_footer(browser) # убираем все в футере - нет доступа к кнопке Submit

        data_fill['First Name'] = forms_page.fill_name(*FormsPage_locators.FIRST_NAME)
        data_fill['Last Name'] = forms_page.fill_name(*FormsPage_locators.LAST_NAME)
        data_fill['Email'] = forms_page.fill_email(*FormsPage_locators.EMAIL)
        data_fill['Gender'] = forms_page.fill_gender(*FormsPage_locators.GENDER)
        data_fill['Mobile'] = forms_page.fill_phone(*FormsPage_locators.MOBILE)
        data_fill['Date of Birth'] = forms_page.fill_date_of_birth(*FormsPage_locators.DATE_OF_BIRTH, Forms.date)
        data_fill['Subjects'] = forms_page.fill_subjects(*FormsPage_locators.SUBJECTS, Forms.name_subject)
        data_fill['Hobbies'] = forms_page.fill_hobbies(*FormsPage_locators.HOBBIES)
        data_fill['Current Address'] = forms_page.fill_address(*FormsPage_locators.CURRENT_ADDRESS_FORMS)
        data_fill['State'] = forms_page.fill_state_or_city(*FormsPage_locators.SELECT_STATE, *FormsPage_locators.STATE_INPUT, *FormsPage_locators.STATE)
        data_fill['Сity'] = forms_page.fill_state_or_city(*FormsPage_locators.SELECT_CITY, *FormsPage_locators.CITY_INPUT, *FormsPage_locators.CITY)
        data_fill['Picture'] = ''

        forms_page.click_element_execute_script(*FormsPage_locators.SUBMIT_FORMS)

        # 3
        data_res = forms_page.data_in_modal(*FormsPage_locators.KEYS, *FormsPage_locators.VALUES)
        res, key = forms_page.compar_data(data_fill, data_res)

        assert res is True, f"Не совпадают данные в блоке {key}"

    '''
    ### Кейс №20: Валидные данные в обязательных полях
    1. Открыть категорию Forms, перейти в Practice Form
    2. Заполнить обязательные поля в форме
    3. Проверить правильность введенных данных во всплывающем модальном окне
    '''
##    @pytest.mark.skip
    def test_required_data_form(self, browser):
        data_fill = dict()

        # 1
        forms_page = FormsPage(browser, LinksPage.LINK_FORMS)
        forms_page.open()

        forms_page.browser.find_element(*FormsPage_locators.PRACTICE_FORM).click()

        # 2
    ##    remove_footer(browser) # убираем все в футере - нет доступа к кнопке Submit

        data_fill['First Name'] = forms_page.fill_name(*FormsPage_locators.FIRST_NAME)
        data_fill['Last Name'] = forms_page.fill_name(*FormsPage_locators.LAST_NAME)
        data_fill['Email'] = ''
        data_fill['Gender'] = forms_page.fill_gender(*FormsPage_locators.GENDER)
        data_fill['Mobile'] = forms_page.fill_phone(*FormsPage_locators.MOBILE)
        data_fill['Date of Birth'] = datetime.datetime.now()
        data_fill['Subjects'] = ''
        data_fill['Hobbies'] = ''
        data_fill['Current Address'] = ''
        data_fill['State'] = ''
        data_fill['Сity'] = ''
        data_fill['Picture'] = ''

        forms_page.click_element_execute_script(*FormsPage_locators.SUBMIT_FORMS)

        # 3
        # нет выделенных полей красным
        assert forms_page.elements_are_novalid_data_fill(*FormsPage_locators.INVALID_INPUT, *FormsPage_locators.INVALID_RADIO), "Поля не должны быть выделены"

        # модальное окно появляется
        assert forms_page.element_is_present(*FormsPage_locators.MODAL_DIALOG), "Модальное окно должно быть"

        # данные соответствуют
        data_res = forms_page.data_in_modal(*FormsPage_locators.KEYS, *FormsPage_locators.VALUES)
        res, key = forms_page.compar_data(data_fill, data_res)

        assert res is True, f"Не совпадают данные в блоке {key}"

    '''
    ### Кейс №21: Невалидные данные в форме
    1. Открыть категорию Forms, перейти в Practice Form
    2. Заполнить все поля невалидными данными (файл загружать не обязательно)
    3. Проверить выделение полей которые были неправильно заполнены

    > p.s. на этот кейс будет не одна проверка
    '''
##    @pytest.mark.skip
    @pytest.mark.parametrize('first_name, last_name, email, gender, mobile', Forms.data_forms)
    def test_novalid_data_form(self, browser, first_name, last_name, email, gender, mobile):

        data_fill = dict()
        # 1
        forms_page = FormsPage(browser, LinksPage.LINK_FORMS)
        forms_page.open()

        forms_page.browser.find_element(*FormsPage_locators.PRACTICE_FORM).click()

        # 2
    ##    remove_footer(browser) # убираем все в футере - нет доступа к кнопке Submit

        data_fill['First Name'] = forms_page.fill_name(*FormsPage_locators.FIRST_NAME, first_name)
        data_fill['Last Name'] = forms_page.fill_name(*FormsPage_locators.LAST_NAME, last_name)
        data_fill['Email'] = forms_page.fill_email(*FormsPage_locators.EMAIL, email)
        data_fill['Gender'] = forms_page.fill_gender(*FormsPage_locators.GENDER, gender)
        data_fill['Mobile'] = forms_page.fill_phone(*FormsPage_locators.MOBILE, mobile)
        data_fill['Date of Birth'] = forms_page.fill_date_of_birth(*FormsPage_locators.DATE_OF_BIRTH, Forms.date)
        data_fill['Subjects'] = forms_page.fill_subjects(*FormsPage_locators.SUBJECTS, Forms.name_subject)
        data_fill['Hobbies'] = forms_page.fill_hobbies(*FormsPage_locators.HOBBIES)
        data_fill['Current Address'] = forms_page.fill_address(*FormsPage_locators.CURRENT_ADDRESS_FORMS)
        data_fill['State'] = forms_page.fill_state_or_city(*FormsPage_locators.SELECT_STATE, *FormsPage_locators.STATE_INPUT, *FormsPage_locators.STATE)
        data_fill['Сity'] = forms_page.fill_state_or_city(*FormsPage_locators.SELECT_CITY, *FormsPage_locators.CITY_INPUT, *FormsPage_locators.CITY)
        data_fill['Picture'] = ''

        forms_page.click_element_execute_script(*FormsPage_locators.SUBMIT_FORMS)

        # 3
        # модального окна нет
        assert forms_page.element_is_present(*FormsPage_locators.MODAL_DIALOG) is False, "Модального окна не должно быть"

        # проверить выделение полей которые были неправильно заполнены
        assert forms_page.elements_are_novalid_data_fill(*FormsPage_locators.INVALID_INPUT, *FormsPage_locators.INVALID_RADIO) is False, "Поле должно быть выделено"
