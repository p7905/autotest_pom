import pytest

from datas.links_page import LinksPage
from methods.AlertsPage import AlertsPage
from locators.AlertsPage_locators import AlertsAlertsPageLocators


class TestAlertsAlertsPage():
    '''
    ## Часть 4: Alerts, Frame & Windows
    ### Кейс №22: Диалоговое окно
    1. Открыть категорию Alerts, Frame & Windows, перейти в Alerts
    2. Нажать кнопку «Click Button to see alert»
    3. Проверить текст в диалоговом окне
    4. Нажать «ОК» в диалоговом окне
    5. Проверить результат
    '''
    def test_dialoge_window(self, browser):
        alert_text = "You clicked a button"

        # 1
        alerts_page = AlertsPage(browser, LinksPage.LINK_ALERTS)
        alerts_page.open()

        alerts_page.browser.find_element(*AlertsAlertsPageLocators.ALERTS).click()

        # 2
        alerts_page.browser.find_element(*AlertsAlertsPageLocators.BUTTON_SEE_ALERT).click()

        # 3
        assert alerts_page.alert_is_present() is True, "Диалоговое окно не появилось"

        alert = alerts_page.browser.switch_to.alert
        assert alert.text == alert_text, f"Текст должен быть '{alert_text}'"

        # 4
        alert.accept()

        # 5
        assert alerts_page.alert_is_present() is False, "Диалоговое окно не закрылось"

    '''
    ### Кейс №23: Диалоговое окно с задержкой
    1. Открыть категорию Alerts, Frame & Windows, перейти в Alerts
    2. Нажать кнопку «On button click, alert will appear after 5 seconds»
    3. Дождаться появления диалогового окна
    4. Проверить текст в диалоговом окне
    5. Нажать «ОК» в диалоговом окне
    6. Проверить результат
    '''
##    @pytest.mark.skip
    def test_dialoge_window_with_wait(self, browser):
        alert_text = "This alert appeared after 5 seconds"

        # 1
        alerts_page = AlertsPage(browser, LinksPage.LINK_ALERTS)
        alerts_page.open()

        alerts_page.browser.find_element(*AlertsAlertsPageLocators.ALERTS).click()

        # 2
        alerts_page.browser.find_element(*AlertsAlertsPageLocators.BUTTON_TIMER_ALERT).click()

        # 3
        assert alerts_page.alert_is_present() is True, "Диалоговое окно не появилось после 5 секунд"

        alert = alerts_page.browser.switch_to.alert
        assert alert.text == alert_text, f"Текст должен быть '{alert_text}'"

        # 4
        alert.accept()

        # 5
        assert alerts_page.alert_is_present() is False, "Диалоговое окно не закрылось"

    '''
    ### Кейс №24: Диалоговое окно с несколькими кнопками
    1. Открыть категорию Alerts, Frame & Windows, перейти в Alerts
    2. Нажать кнопку «Click Button to see alert» - ?
    скорее всего "On button click, confirm box will appear"
    3. Проверить текст в диалоговом окне
    4. Нажать «Cancel» в диалоговом окне
    5. Проверить результат
    '''
##    @pytest.mark.skip
    def test_dialoge_window_with_buttons(self, browser):
        alert_text = "Do you confirm action?"

        # 1
        alerts_page = AlertsPage(browser, LinksPage.LINK_ALERTS)
        alerts_page.open()

        alerts_page.browser.find_element(*AlertsAlertsPageLocators.ALERTS).click()

        # 2
        ##    2. Нажать кнопку «Click Button to see alert» - ?
        ##    скорее всего "On button click, confirm box will appear"
        alerts_page.browser.find_element(*AlertsAlertsPageLocators.BUTTON_CONFIRM).click()

        # 3
        assert alerts_page.alert_is_present() is True, "Диалоговое окно не появилось"

        alert = alerts_page.browser.switch_to.alert
        assert alert.text == alert_text, f"Текст должен быть '{alert_text}'"

        # 4
        alert.dismiss()

        # 5
        assert alerts_page.alert_is_present() is False, "Диалоговое окно не закрылось"
        assert alerts_page.element_is_present(*AlertsAlertsPageLocators.CONFIRM_RESULT) is True, "Нет текста результата"

        result_text = alerts_page.browser.find_element(*AlertsAlertsPageLocators.CONFIRM_RESULT).text

        assert 'Cancel' in result_text, f"В результате должен присутствовать текст 'Cancel'"

    '''
    ### Кейс №25: Диалоговое окно с вводом данных
    1. Открыть категорию Alerts, Frame & Windows, перейти в Alerts
    2. Нажать кнопку «On button click, prompt box will appear»
    3. Проверить текст в диалоговом окне
    4. В поле ввода текста ввести своё имя и нажать «ОК»
    5. Проверить результат
    '''
##    @pytest.mark.skip
    def test_dialoge_window_with_input(self, browser):
        alert_text = "Please enter your name"
        name = "Anna"

        # 1
        alerts_page = AlertsPage(browser, LinksPage.LINK_ALERTS)
        alerts_page.open()

        alerts_page.browser.find_element(*AlertsAlertsPageLocators.ALERTS).click()

        # 2
        alerts_page.browser.find_element(*AlertsAlertsPageLocators.BUTTON_PROMPT).click()

        # 3
        assert alerts_page.alert_is_present() is True, "Диалоговое окно не появилось"

        alert = alerts_page.browser.switch_to.alert
        assert alert.text == alert_text, f"Текст должен быть '{alert_text}'"

        # 4
        alert.send_keys(name)
        alert.accept()

        # 5
        assert alerts_page.alert_is_present() is False, "Диалоговое окно не закрылось"
        assert alerts_page.element_is_present(*AlertsAlertsPageLocators.PROMPT_RESULT) is True, "Нет текста результата"

        result_text = alerts_page.browser.find_element(*AlertsAlertsPageLocators.PROMPT_RESULT).text
        assert name in result_text, f"В результате должен присутствовать текст {name}"
