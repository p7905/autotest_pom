import pytest
import random

from selenium.webdriver.common.by import By
from selenium.common import exceptions as E
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from datas.links_page import LinksPage
from datas.data_example import CheckBox
from methods.ElementsPage import ElementsPage

from locators.ElementsPage_locators import *

list_name_Request = [['Created', '201', LinksElPageLocators.CREATED_REQ],
                    ['No Content', '204', LinksElPageLocators.NO_CONTENT_REQ],
                    ['Moved Permanently', '301', LinksElPageLocators.MOVED_REQ],
                    ['Bad Request', '400', LinksElPageLocators.BAD_REQ],
                    ['Unauthorized', '401', LinksElPageLocators.UNAUTHORIZED_REQ],
                    ['Forbidden', '403', LinksElPageLocators.FORBIDDEN_REQ],
                    ['Not Found', '404', LinksElPageLocators.NOT_FOUND_REQ]]

##@pytest.mark.skip
class TestTextBoxElementsPage():
    '''
    ## Часть 2: Elements
    ### Кейс №4: Валидные данные Text Box
    1. Открыть категорию Elements, перейти в Text Box
    2. Заполнить поле(-я) валидными данными
    3. Проверить отправку данных в появившемся блоке под формой
    '''

    def test_textbox_valid_data(self, browser):

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.browser.find_element(*TextBoxElPageLocators.TEXT_BOX).click()

        #2
        input_data, count_fill_fields = elements_page.generate_random_data()

        elements_page.browser.find_element(*TextBoxElPageLocators.FULL_NAME).send_keys(input_data['Full Name'])
        elements_page.browser.find_element(*TextBoxElPageLocators.EMAIL).send_keys(input_data['Email'])
        elements_page.browser.find_element(*TextBoxElPageLocators.CUR_ADDRESS).send_keys(input_data['Current Address'])
        elements_page.browser.find_element(*TextBoxElPageLocators.PERM_ADDRESS).send_keys(input_data['Permanent Address'])

        elements_page.submit_data(*TextBoxElPageLocators.BUTTON_SUBMIT)

        #3
        ##    строки выхоного блока есть
        assert elements_page.element_is_visible(*TextBoxElPageLocators.ELEMENTS_OUTPUT_BLOCK), "Вывод данных под формой отсутствует"
        ##    кол-во совпадает
        result_data = elements_page.browser.find_elements(*TextBoxElPageLocators.ELEMENTS_OUTPUT_BLOCK)
        assert len(result_data) == count_fill_fields, f"Выведено {len(result_data)} строк, вместо {count_fill_fields}"
        ##    данные совпадают
        str_data = ''.join([elem.text.split(':')[1] for elem in result_data])
        str_res = ''.join(input_data.values())
        assert str_res == str_data, f"В выведенных данных {str_res} ожидались другие данные"

    '''
    ### Кейс №5: Невалидные данные Text Box
    1. Открыть категорию Elements, перейти в Text Box
    2. Заполнить поле(-я) невалидными данными
    3. Проверить отсутствие данных под формой и выделение поля(-ей) с ошибками
    '''
    # email - невалидное, остальные тип текстовый, все равно что туда запихивать
    def test_textbox_novalid_data(self, browser):
        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.browser.find_element(*TextBoxElPageLocators.TEXT_BOX).click()

        #2
        input_data, count_fill_fields = elements_page.generate_random_data(email=False)

        elements_page.browser.find_element(*TextBoxElPageLocators.FULL_NAME).send_keys(input_data['Full Name'])
        elements_page.browser.find_element(*TextBoxElPageLocators.EMAIL).send_keys(input_data['Email'])
        elements_page.browser.find_element(*TextBoxElPageLocators.CUR_ADDRESS).send_keys(input_data['Current Address'])
        elements_page.browser.find_element(*TextBoxElPageLocators.PERM_ADDRESS).send_keys(input_data['Permanent Address'])

        elements_page.submit_data(*TextBoxElPageLocators.BUTTON_SUBMIT)

        #3
        # строк выходного блока нет
        assert elements_page.element_is_not_visible(*TextBoxElPageLocators.ELEMENTS_OUTPUT_BLOCK), "Вывод данных под формой должен отсутсвовать"
        # есть обозначения невалидных данных - красная рамка
        assert elements_page.element_is_visible(*TextBoxElPageLocators.ERROR_ELEMENTS), "Нет обозначения, что введены невалидные значения"

##@pytest.mark.skip
class TestCheckBoxElementsPage():
    '''
    ### Кейс №6: Выделение нужных чекбоксов
    1. Открыть категорию Elements, перейти в Check Box
    2. Выделить следующие чекбоксы: Notes, Angular, General, Word File.doc
    3. Проверить выделенные чекбоксы в блоке «You have selected»
    '''
    def test_activate_checkbox(self, browser):
        list_item_check_box = []
        list_item_check_box.extend(CheckBox.list_item_check_box)

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.browser.find_element(*CheckBoxElPageLocators.CHECK_BOX).click()

        # 2
        elements_page.browser.find_element(*CheckBoxElPageLocators.BUTTON_EXPAND_ALL).click()
        for elem in list_item_check_box:
            locator_elem = f"//span[@class='rct-title' and contains(text(), '{elem}')]"
            elements_page.go_to_and_click(By.XPATH, locator_elem)

        # 3
        # блок присутсвует
        assert elements_page.element_is_visible(*CheckBoxElPageLocators.CHECK_RESULT), "Блок «You have selected» отсутствует на странице"
        assert elements_page.element_is_visible(*CheckBoxElPageLocators.LIST_CHECK_RESULT), "Список отмеченных элемнтов отсутствует на странице"
        # в блоке нужные данные
        list_check_result = [elem.text for elem in elements_page.browser.find_elements(*CheckBoxElPageLocators.LIST_CHECK_RESULT)]

        str_data = ''.join(list_item_check_box).replace('.doc', '').replace(' ', '').upper()
        str_res = ''.join(list_check_result).upper()

        assert str_data == str_res, "В блоке «You have selected» отображется неверный текст"

##@pytest.mark.skip
class TestRadioButtonElementsPage():
    '''
    ### Кейс №7: Выделение нужных радио-кнопок
    1. Открыть категорию Elements, перейти в Radio Button
    2. Выделить Yes
    3. Проверить выделенную кнопку в блоке «You have selected»
    4. Проверить что кнопку No невозможно выделить
    5. Выделить Impressive
    6. Проверить выделенную кнопку в блоке «You have selected»
    ##'''
    def test_activate_radiobatton(self, browser):
        name_attribute = "disabled"

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.browser.find_element(*RadioButtonElPageLocators.RADIO_BUTTON).click()

        # 2
        Yes_radio, Yes_radio_ch = elements_page.checking_radio_button(*RadioButtonElPageLocators.YES_BUTTON,
                                                                      *RadioButtonElPageLocators.YES_CHECK)

        # 3
        str_yes = Yes_radio.text
        str_res = elements_page.browser.find_element(*RadioButtonElPageLocators.RADIO_CHECK_RESULT).text
        assert not elements_page.element_is_include_attribute(*RadioButtonElPageLocators.YES_CHECK, name_attribute), \
                    "Ожидалось, что элемент {str_yes} можно выбрать"
        assert elements_page.element_is_visible(*RadioButtonElPageLocators.RADIO_RESULT), \
                    "Блок «You have selected» отсутствует на странице"
        assert str_res == str_yes, f"Отображается текст {str_yes}, вместо {str_res}"

        # 4
        No_radio, No_radio_ch = elements_page.checking_radio_button(*RadioButtonElPageLocators.NO_BUTTON,
                                                                    *RadioButtonElPageLocators.NO_CHECK)

        str_no = No_radio.text
        assert elements_page.element_is_include_attribute(*RadioButtonElPageLocators.NO_CHECK, name_attribute), \
                    "Ожидалось, что элемент {str_no} нельзя выбрать"
        assert elements_page.element_is_visible(*RadioButtonElPageLocators.RADIO_RESULT), \
                    "Блок «You have selected» должен отсутствовать на странице"

        # 5
        Impressive_radio, Impressive_radio_ch = elements_page.checking_radio_button(
                                                *RadioButtonElPageLocators.IMPRESSIVE_BUTTON,
                                                *RadioButtonElPageLocators.IMPRESSIVE_CHECK)

        # 6
        str_impressive = Impressive_radio.text
        str_res = elements_page.browser.find_element(*RadioButtonElPageLocators.RADIO_CHECK_RESULT).text
        assert not elements_page.element_is_include_attribute(*RadioButtonElPageLocators.IMPRESSIVE_CHECK, name_attribute), \
                    "Ожидалось, что элемент {str_impressive} можно выбрать"
        assert elements_page.element_is_visible(*RadioButtonElPageLocators.RADIO_RESULT), \
                    "Блок «You have selected» отсутствует на странице"
        assert str_res == str_impressive, f"Отображается текст {str_impressive}, вместо {str_res}"

##@pytest.mark.skip
class TestWebTablesElementsPage():
    '''
    ### Кейс №8: Добавление записи в таблицу
    1. Открыть категорию Elements, перейти в Web Tables
    2. Добавить новую запись в таблицу с валидными данными
    3. Проверить добавление записи согласно введенным данным
    '''
    def test_add_row_in_table(self, browser):
        input_data_table = dict()

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open()

        elements_page.go_to_and_click(*WebTablesElPageLocators.WEB_TABLES)

        # 2
        elements_page.browser.find_element(*WebTablesElPageLocators.ADD_ROW_BUTTON).click()
        elements_page.browser.find_element(*WebTablesElPageLocators.USER_FORM)

        input_data_table = elements_page.generate_random_data_table()

        elements_page.browser.find_element(*WebTablesElPageLocators.FIRST_NAME).send_keys(input_data_table['First Name'])
        elements_page.browser.find_element(*WebTablesElPageLocators.LAST_NAME).send_keys(input_data_table['Last Name'])
        elements_page.browser.find_element(*WebTablesElPageLocators.AGE).send_keys(input_data_table['Age'])
        elements_page.browser.find_element(*WebTablesElPageLocators.USER_EMAIL).send_keys(input_data_table['User Email'])
        elements_page.browser.find_element(*WebTablesElPageLocators.SALARY).send_keys(input_data_table['Salary'])
        elements_page.browser.find_element(*WebTablesElPageLocators.DEPARTMENT).send_keys(input_data_table['Department'])

        elements_page.browser.find_element(*WebTablesElPageLocators.ADD_USER_SUBMIT).click()

        # 3
        rows_table = elements_page.browser.find_elements(*WebTablesElPageLocators.ROW)
        row_output = [item.text.splitlines() for item in rows_table]
        row_input = list(input_data_table.values())
        assert row_input in row_output, "В таблице нет добавленной строки"


    '''
    ### Кейс №9: Редактирование записей в таблице
    1. Открыть категорию Elements, перейти в Web Tables
    2. Заменить данные первой записи в таблице на данные из третьей записи
    3. Заменить данные второй записи в таблице на старые данные из первой записи
    4. Заменить данные третьей записи в таблице на старые данные из второй записи
    5. Проверить содержание записей согласно их новому расположению

    > p.s. если коротко, то записи меняются так 3=>1=>2=>3
    '''
    def test_edit_row_in_table(self, browser):
        key_dict = ['First Name', 'Last Name', 'Age', 'User Email', 'Salary', 'Department']

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.go_to_and_click(*WebTablesElPageLocators.WEB_TABLES)

        # 2 -4
        rows = [item.text.splitlines() for item in browser.find_elements(*WebTablesElPageLocators.ROW)]
        row_new = list()

        row_new.append(rows[2])
        row_new.append(rows[0])
        row_new.append(rows[1])

        for row in range(len(row_new)):
            input_data_table = dict(zip(key_dict, row_new[row]))

            elements_page.browser.find_elements(*WebTablesElPageLocators.EDIT_BUTTON)[row].click()

            elements_page.edit_data_in_table(*WebTablesElPageLocators.FIRST_NAME, input_data_table['First Name'])
            elements_page.edit_data_in_table(*WebTablesElPageLocators.LAST_NAME,input_data_table['Last Name'])
            elements_page.edit_data_in_table(*WebTablesElPageLocators.AGE, input_data_table['Age'])
            elements_page.edit_data_in_table(*WebTablesElPageLocators.USER_EMAIL, input_data_table['User Email'])
            elements_page.edit_data_in_table(*WebTablesElPageLocators.SALARY, input_data_table['Salary'])
            elements_page.edit_data_in_table(*WebTablesElPageLocators.DEPARTMENT, input_data_table['Department'])

            elements_page.browser.find_element(*WebTablesElPageLocators.ADD_USER_SUBMIT).click()

        # 5
        rows = [item.text.splitlines() for item in elements_page.browser.find_elements(*WebTablesElPageLocators.ROW)]

        for i in range(len(row_new)):
            assert row_new[i] == rows[i], f"Должно быть {row_new[i]}, вместо {rows[i]}"


    '''
    ### Кейс №10: Удаление записи в таблице
    1. Открыть категорию Elements, перейти в Web Tables
    2. Удалить одну из записей в таблице
    3. Проверить изменения таблицы
    '''
    def test_delete_row_in_table(self, browser):

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open()

        elements_page.go_to_and_click(*WebTablesElPageLocators.WEB_TABLES)

        # 2
        row_nom = random.randint(0,2)
        row_del = elements_page.browser.find_elements(*WebTablesElPageLocators.ROW)[row_nom].text.splitlines()
        elements_page.browser.find_elements(*WebTablesElPageLocators.DELETE_BUTTON)[row_nom].click()

        # 3
        row_new = [item.text.splitlines() for item in elements_page.browser.find_elements(*WebTablesElPageLocators.ROW)]

        assert row_del not in row_new, f"Строка {row_del} не удалена из таблицы"

    '''
    ### Кейс №11: Пагинация таблицы через кнопки
    1. Открыть категорию Elements, перейти в Web Tables
    2. Добавить такое количество записей, чтобы их было >5
    3. Переключить отображение только для 5 записей
    4. Проверить наличие записей согласно введенным данным, переключив страницу на следующую
    '''
    def test_pagination_with_button_in_table(self, browser):

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open()

        elements_page.go_to_and_click(*WebTablesElPageLocators.WEB_TABLES)

        # 2
        rows_input = list()
        count_add_row = random.randint(3,5)

        for row in range(count_add_row):
            elements_page.browser.find_element(*WebTablesElPageLocators.ADD_ROW_BUTTON).click()
            elements_page.browser.find_element(*WebTablesElPageLocators.USER_FORM)

            input_data_table = elements_page.generate_random_data_table()

            elements_page.browser.find_element(*WebTablesElPageLocators.FIRST_NAME).send_keys(input_data_table['First Name'])
            elements_page.browser.find_element(*WebTablesElPageLocators.LAST_NAME).send_keys(input_data_table['Last Name'])
            elements_page.browser.find_element(*WebTablesElPageLocators.AGE).send_keys(input_data_table['Age'])
            elements_page.browser.find_element(*WebTablesElPageLocators.USER_EMAIL).send_keys(input_data_table['User Email'])
            elements_page.browser.find_element(*WebTablesElPageLocators.SALARY).send_keys(input_data_table['Salary'])
            elements_page.browser.find_element(*WebTablesElPageLocators.DEPARTMENT).send_keys(input_data_table['Department'])

            elements_page.browser.find_element(*WebTablesElPageLocators.ADD_USER_SUBMIT).click()

            rows_input.append(list(input_data_table.values()))

        rows_page_next = rows_input[2:]

        # 3
        elements_page.select_per_page(*WebTablesElPageLocators.SELECT_PER_PAGE, 5)

        # 4
        elements_page.browser.find_element(*WebTablesElPageLocators.NEXT_PAGE).click()
        rows_page = [item.text.splitlines() for item in elements_page.browser.find_elements(*WebTablesElPageLocators.ROW)]

        for i in range(len(rows_page_next)):
            assert rows_page_next[i] == rows_page[i], f"Должно быть {rows_page_next[i]}, вместо {rows_page[i]}"


    '''
    ### Кейс №12: Пагинация таблицы через значения
    1. Открыть категорию Elements, перейти в Web Tables
    2. Добавить такое количество записей, чтобы их было >5
    3. Переключить отображение только для 5 записей
    4. Переключить страницу отображения записей на следующую
    5. Указать номер страницы «1» и перейти
    6.  Проверить наличие записей согласно введенным данным
    '''
    def test_pagination_with_values_in_table(self, browser):

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.go_to_and_click(*WebTablesElPageLocators.WEB_TABLES)

        # 2
        rows_input = list()
        count_add_row = random.randint(3,5)

        for row in range(count_add_row):
            elements_page.browser.find_element(*WebTablesElPageLocators.ADD_ROW_BUTTON).click()
            elements_page.browser.find_element(*WebTablesElPageLocators.USER_FORM)

            input_data_table = elements_page.generate_random_data_table()

            elements_page.browser.find_element(*WebTablesElPageLocators.FIRST_NAME).send_keys(input_data_table['First Name'])
            elements_page.browser.find_element(*WebTablesElPageLocators.LAST_NAME).send_keys(input_data_table['Last Name'])
            elements_page.browser.find_element(*WebTablesElPageLocators.AGE).send_keys(input_data_table['Age'])
            elements_page.browser.find_element(*WebTablesElPageLocators.USER_EMAIL).send_keys(input_data_table['User Email'])
            elements_page.browser.find_element(*WebTablesElPageLocators.SALARY).send_keys(input_data_table['Salary'])
            elements_page.browser.find_element(*WebTablesElPageLocators.DEPARTMENT).send_keys(input_data_table['Department'])

            elements_page.browser.find_element(*WebTablesElPageLocators.ADD_USER_SUBMIT).click()

            rows_input.append(list(input_data_table.values()))

        rows_page_prev = rows_input[0:2]

        # 3
        elements_page.select_per_page(*WebTablesElPageLocators.SELECT_PER_PAGE, 5)

        # 4
        elements_page.browser.find_element(*WebTablesElPageLocators.NEXT_PAGE).click()

        # 5
        elements_page.go_to_page(*WebTablesElPageLocators.PAGE_VALUE, 1)

        # 6
        rows_page = [item.text.splitlines() for item in elements_page.browser.find_elements(*WebTablesElPageLocators.ROW)]

        for i in range(len(rows_page_prev)):
            assert rows_page_prev[i] == rows_page[i + 3], f"Должно быть {rows_page_prev[i]}, вместо {rows_page[i]}"

##@pytest.mark.skip
class TestButtonsElementsPage():
    '''
    ### Кейс №13: Клики по кнопкам
    1. Открыть категорию Elements, перейти в Buttons
    2. Поочередно нажать кнопки: Right Click, Click, Double Click
    3. Проверить нажатие кнопок в блоке под ними «You have done»
    '''
    def test_button_clicks(self, browser):

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.go_to_and_click(*ButtonsElPageLocators.BUTTONS)

        # 2
        elements_page.clicks_element("double", *ButtonsElPageLocators.DBL_CLICK_BUT)
        elements_page.clicks_element("right", *ButtonsElPageLocators.RGH_CLICK_BUT)
        # чтобы было в едином стиле, но вполне можно просто кликнуть
        elements_page.clicks_element("click", *ButtonsElPageLocators.CLICK_BUT)
        # elements_page.browser.find_element(*ButtonsElPageLocators.CLICK_BUT).click()

        # 3
        assert elements_page.element_is_present(*ButtonsElPageLocators.DBL_CLICK_MESSAGE), \
                                "Блок «You have done a double click» отсутствует на странице"
        assert elements_page.element_is_present(*ButtonsElPageLocators.RGH_CLICK_MESSAGE), \
                                "Блок «You have done a right click» отсутствует на странице"
        assert elements_page.element_is_present(*ButtonsElPageLocators.CLICK_MESSAGE), \
                                "Блок «You have done a dynamic click» отсутствует на странице"

##@pytest.mark.skip
class TestLinksElementsPage():
    '''
    ### Кейс №14: Новая вкладка
    1. Открыть категорию Elements, перейти в Links
    2. Перейти по ссылке «Home», откроется новая вкладка
    3. Проверить, что текущее состояние адресной строки соответствует «https://demoqa.com/»
    4. Переключиться на первую вкладку (не закрывая новую!) и проверить,
    что текущее состояние адресной строки соответствует «https://demoqa.com/links»
    '''
    def test_links_new_page(self, browser):

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.go_to_and_click(*LinksElPageLocators.LINKS)

        # 2
        home_link = elements_page.browser.find_element(*LinksElPageLocators.HOME_LINK).click()

        # 3
        url_home = elements_page.get_url_page(1)
        assert url_home == "https://demoqa.com/"

        # 4
        url_links = elements_page.get_url_page(0)
        assert url_links == "https://demoqa.com/links"


    '''
    ### Кейс №15 (API): Проверка статусов запросов
    1. Открыть категорию Elements, перейти в Links
    2. Проверить правильность работы каждого запроса (Created, No Content, Moved, Bad Request, Unauthorized, Forbidden, Not Found):
    3. Кликнуть на линк
    4. Получить запрос и проверить его имя и код статуса
    не делайте через библиотеку requests, т.е. не через запросы.
    на данном этапе просто смотрите, что появилась надпись "Link has responded with staus 201 and status text Created".
    '''
    @pytest.mark.parametrize('name_Request', list_name_Request)
    def test_links_status_network(self, browser, name_Request):

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.go_to_and_click(*LinksElPageLocators.LINKS)

        # 2
        # если не доскроллить до элемента, то работает нестабильно
        elements_page.go_to_and_click(*name_Request[2])

        # 3
        assert elements_page.element_is_visible(*LinksElPageLocators.RESPONSE_MESSAGE), \
                    f"Сообщения с кодом и статусом нет {name_Request[0]}"
        response = elements_page.browser.find_elements(*LinksElPageLocators.RESPONSE_MESSAGE)
        assert name_Request[1] == response[0].text and name_Request[0] == response[1].text, \
                    f"Ожидалось {name_Request[1]} - {name_Request[1]}, вместо {response[0].text}{response[1].text}"

##@pytest.mark.skip
class TestBrokenLinksElementsPage():
    '''
    ### Кейс №16: Картинки
    1. Открыть категорию Elements, перейти в Broken Links — Images
    2. Проверить отображение картинки блока Valid image
    3. Проверить неотображение картинки блока Broken image
    '''
    def test_images(self, browser):

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.go_to_and_click(*BrokenLinksElPageLocators.BROKEN_LINKS)

        # 2
        valid_image = elements_page.browser.find_elements(*BrokenLinksElPageLocators.IMAGE)[0]
        naturalWidth = elements_page.get_naturalWidth_img(valid_image)
        assert naturalWidth != 0, "Ожидалoсь, что картинка отобразится"

        # 3
        broken_image = elements_page.browser.find_elements(*BrokenLinksElPageLocators.IMAGE)[1]
        naturalWidth = elements_page.get_naturalWidth_img(broken_image)
        assert naturalWidth == 0, "Ожидалoсь, что картинка не отобразится"

    '''
    ### Кейс №17: Ссылка
    1. Открыть категорию Elements, перейти в Broken Links — Images
    2.  Нажать на ссылку «Click Here for Broken Link»
    3. Проверить переход на новую страницу и её статус (500)
    4. Вернуться назад на предыдущую страницу
    5. Нажать на ссылку «Click Here for Valid Link»
    6. Проверить, что текущее состояние адресной строки соответствует «https://demoqa.com/»
    '''
    def test_broken_links(self, browser):

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.go_to_and_click(*BrokenLinksElPageLocators.BROKEN_LINKS)

        url = elements_page.get_url_page(0)

        # 2
        broken_link = elements_page.browser.find_element(*BrokenLinksElPageLocators.BROKEN_LINK).click()
        new_url = elements_page.get_url_page(0)

        # 3
        assert new_url != url, "Ожидался переход на новую страницу"
        # не очень понятно откуда этот статус (500) вытаскивать
        assert '500' in new_url, "В адресе ожидался статус 500"

        # 4
        elements_page.open(url)

        # 5
        valid_link = elements_page.browser.find_element(*BrokenLinksElPageLocators.VALID_LINK).click()
        new_url = elements_page.get_url_page(0)

        # 6
        assert new_url == "https://demoqa.com/", "Ожидался переход на страницу 'https://demoqa.com/'"

##@pytest.mark.skip
class TestDynamicPropertiesElementsPage():
    '''
    ### Кейс №18: Динамические свойства объектов
    1. Открыть категорию Elements, перейти в Dynamic Properties
    2.  Проверить состояние кнопок:
    	2.1 Кнопка «Will enable 5 seconds» должна быть неактивной первые пять секунд
    	2.2 В кнопке «Color Change» должен измениться цвет текста через пять секунд
    	2.3 Кнопка «Visible after 5 seconds» должна быть невидимой первые пять секунд
     '''
    def test_dynamic_properties(self, browser):

        # 1
        elements_page = ElementsPage(browser, LinksPage.LINK_ELEMENTS)
        elements_page.open() # нужно или можно в ините сделать открытие стрaницы????

        elements_page.go_to_and_click(*DynamicPropertiesElPageLocators.DYNAMIC_PROPERTIES)

        # 2
        res = True
        try:
            WebDriverWait(elements_page.browser, 6).until(EC.all_of(EC.element_to_be_clickable(DynamicPropertiesElPageLocators.BUTTON_ENABLED_AFTER),
                EC.visibility_of_element_located(DynamicPropertiesElPageLocators.BUTTON_VISIBLE_AFTER),
                EC.text_to_be_present_in_element_attribute(DynamicPropertiesElPageLocators.BUTTON_COLOR_CHANGE, 'class', 'text-danger')))
        except E.TimeoutException:
            res = False

        assert res is True, "Что-то пошло не так"