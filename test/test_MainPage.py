import pytest

from datas.links_page import LinksPage
from methods.MainPage import MainPage
from locators.MainPage_locators import MainPageLocators

# пока не знаю куда перенести
list_elements = [[MainPageLocators.CARD_ELEMENTS, 'Elements'],
                [MainPageLocators.CARD_FORMS, 'Forms'],
                [MainPageLocators.CARD_ALERTS, 'Alerts'],
                [MainPageLocators.CARD_WIDGETS, 'Widgets'],
                [MainPageLocators.CARD_INTERACTIONS, 'Interaction'],
                [MainPageLocators.CARD_BOOK, 'Book']]


'''
Кейс №1-3: объединяем все в один

    1. Перейти на главную страницу
    2. Проверяем наличие лого, картинки, ссылки и ссылка должна совпадать с главной страницей
    3. Проверить наличие шести категорий: Elements, Forms, Alerts, Widgets, Interactions, Book
        (проверяем по списку наличие плитки, клик на нее, возврат по лого на главную)

'''
class TestMainPage():
    # вместо 3-х отдельных, один - типа Е2Е
    def test_unoin_main_page(self, browser):
        global list_elements

        # 1
        main_page = MainPage(browser, LinksPage.LINK_MAIN_PAGE)
        main_page.open() # нужно или можно в ините сделать открытие стрaницы????

        #2
        assert main_page.element_is_present(*MainPageLocators.LOGO_MAIN_PAGE) is True, "Логотип на странице отсутствует"

        href = main_page.get_attribute_for_element(*MainPageLocators.LOGO_MAIN_PAGE_LINK, "href")
        assert href == LinksPage.LINK_MAIN_PAGE, f'Для перехода по логотипу указано {href}, вместо {LinksPage.LINK_MAIN_PAGE}'

        # 3
        for k in range(len(list_elements)):
            assert main_page.element_is_present(*list_elements[k][0]) is True, f'{list_elements[k][1]} на странице отсутствует'

            url_new = main_page.get_url_after_go_to_on_card(*list_elements[k][0])
            assert list_elements[k][1].upper() in url_new.upper(), f'Переход по ссылке {url_new}, вместо категории {list_elements[k][1]}'

            url_new = main_page.get_url_after_go_to_on_card(*MainPageLocators.LOGO_MAIN_PAGE_LINK)
            assert url_new == LinksPage.LINK_MAIN_PAGE, f'Переход по логотипу на {url_new}, вместо {LinksPage.LINK_MAIN_PAGE}'
