import pytest

import random

from datas.links_page import LinksPage
from methods.InteractionsPage import InteractionsPage
from datas.data_example import Sortable

from locators.InteractionsPage_locators import *

class TestSortableInteractionsPage:
    '''
    ## Часть 7: Interactions - 4
    ### Кейс №39: Сортировка списка
    1. Открыть категорию Interactions, перейти в Sortable
    2. Во вкладке List провести сортировку списка (от верхнего к нижнему): Six Four Two Five Three One (6 4 2 5 3 1)
    3. Проверить полученный список
    '''
##    @pytest.mark.skip
    def test_sortable(self, browser):
        # 1
        interactions_page = InteractionsPage(browser, LinksPage.LINK_INTERACTIONS)
        interactions_page.open()

        interactions_page.go_to_and_click(*SortableInteractionsPageLocators.SORTABLE)

        # 2
        interactions_page.browser.find_element(*SortableInteractionsPageLocators.LIST_SORTABLE).click()

        # 3
        for i in range(len(Sortable.list_sort)):
            list_items = interactions_page.browser.find_elements(*SortableInteractionsPageLocators.LIST_ITEMS)

            index_elem = interactions_page.get_index_list(list_items, Sortable.list_sort[i])
            assert index_elem != -1, f"Элемент {list_sort[i]} не найден"

            interactions_page.drag_and_drop_element(list_items[index_elem], list_items[i])

        list_items = [elem.text for elem in interactions_page.browser.find_elements(*SortableInteractionsPageLocators.LIST_ITEMS)]

        assert Sortable.list_sort == list_items, "Сортировка неверная"

class TestSelectableInteractionsPage:
    '''
    ### Кейс №40: Выбор строк
    1. Открыть категорию Interactions, перейти в Selectable
    2. Во вкладке List выбрать от 1 до 3 строк
    3. Проверить выделение этих строк
    '''
##    @pytest.mark.skip
    def test_selectable(self, browser):
        list_item_select = []

        # 1
        interactions_page = InteractionsPage(browser, LinksPage.LINK_INTERACTIONS)
        interactions_page.open()

        interactions_page.go_to_and_click(*SelectableInteractionsPageLocators.SELECTABLE)

        # 2
        interactions_page.browser.find_element(*SelectableInteractionsPageLocators.LIST_SELECTABLE).click()
        count_select = random.randint(1, 3)

        list_no_select = interactions_page.browser.find_elements(*SelectableInteractionsPageLocators.LIST_NO_SELECT)
        for i in range(count_select):
            list_no_select[i].click()

            list_item_select.append(list_no_select[i].text)

        # 3
        list_select = [elem.text for elem in interactions_page.browser.find_elements(*SelectableInteractionsPageLocators.LIST_SELECT)]

        assert list_item_select == list_select, "Выделенны неверные строки"
        assert count_select == len(list_select), f"Выделено {count_select} строк вместо {len(list_select)}"

class TestDroppableInteractionsPage:
    '''
    ### Кейс №41: Перетаскивание в блоки
    1. Открыть категорию Interactions, перейти в Droppable
    2. Перейти во вкладку Prevent Propogation
    3. Перетащить блок Drag Me во внутренний блок Inner droppable (greedy) нижнего большого блока
    4. Проверить выделение блока
    '''
##    @pytest.mark.skip
    def test_droppable(self, browser):
        # 1
        interactions_page = InteractionsPage(browser, LinksPage.LINK_INTERACTIONS)
        interactions_page.open()

        interactions_page.go_to_and_click(*DroppableInteractionsPageLocators.DROPPABLE)

        # 2
        interactions_page.browser.find_element(*DroppableInteractionsPageLocators.PREVENT_PROPOGATION).click()

        # 3
        interactions_page.drag_and_drop_element(interactions_page.browser.find_element(*DroppableInteractionsPageLocators.DRAG_BOX),
                                                interactions_page.browser.find_element(*DroppableInteractionsPageLocators.DROP_BOX_INNER))

        # 4
        assert interactions_page.element_is_visible(*DroppableInteractionsPageLocators.DROP_BOX_INNER_COLOR) is True, "Блок не выделился"

class TestDragabbleInteractionsPage:
    '''
    ### Кейс №42: Перетаскивание внутри блоков
    1. Открыть категорию Interactions, перейти в Dragabble
    2. Перейти во вкладку Container Restricted
    3. Переместить блок «I'm contained within the box» в правую нижнюю часть его родительского блока (большой верхний блок, его id="containmentWrapper")
    4. Проверить перемещение блока «I'm contained within the box»
    '''
##    @pytest.mark.skip
    def test_dragabble(self, browser):

        # 1
        interactions_page = InteractionsPage(browser, LinksPage.LINK_INTERACTIONS)
        interactions_page.open()

        interactions_page.go_to_and_click(*DragabbleInteractionsPageLocators.DRAGABBLE)

        # 2
        interactions_page.browser.find_element(*DragabbleInteractionsPageLocators.CONTAINER_RESTRICTION).click()

        # 3
        big_bloc = interactions_page.browser.find_element(*DragabbleInteractionsPageLocators.BIG_BLOC)
        y_coor = big_bloc.size["height"]

        small_bloc = interactions_page.browser.find_element(*DragabbleInteractionsPageLocators.SMALL_BLOC)
        location_old = small_bloc.location

        interactions_page.drag_and_drop_by_offset(small_bloc, y=y_coor)
        location_new = small_bloc.location

        # 4
        assert location_old['x'] == location_new['x'], "По горизонтали блок не должен был двигаться"
        assert location_new['y'] != location_old['y'] , "Блок должен был двигаться по вертикали"
