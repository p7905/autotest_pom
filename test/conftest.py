import pytest
import time
import datetime
import os
from selenium import webdriver

@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # set a report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"

    setattr(item, "rep_" + rep.when, rep)

@pytest.fixture
def browser(request):
    print("\nstart browser for test..")
    browser = webdriver.Chrome()
    browser.maximize_window()

    yield browser

    print("\nquit browser..")
    browser.quit()

@pytest.fixture(scope="function", autouse=True)
def test_failed_check(request, browser):
    path_fail = "failed_reports"
    path_pass = "passed_reports"

    yield
    # request.node is an "item" because we use the default
    # "function" scope
    if request.node.rep_setup.failed:
        print("setting up a test failed!", request.node.nodeid)
    elif request.node.rep_setup.passed:
        if request.node.rep_call.failed:
            if not os.path.isdir(path_fail):
                os.mkdir(path_fail)
            take_screenshot(browser, request.node.nodeid, path_fail)
            print("executing test failed", request.node.nodeid)
        if request.node.rep_call.passed:
            if not os.path.isdir(path_pass):
                os.mkdir(path_pass)
            take_screenshot(browser, request.node.nodeid, path_pass)
            print("executing test passed", request.node.nodeid)

# make a screenshot with a name of the test, date and time
def take_screenshot(browser, nodeid, path):
    time.sleep(1)

    now = f"{nodeid}_{datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.png".replace("/","_").replace("::","__")
    browser.save_screenshot(f"{path}/{now}")    