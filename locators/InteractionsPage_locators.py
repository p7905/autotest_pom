from selenium.webdriver.common.by import By

class SortableInteractionsPageLocators:
    SORTABLE = (By.XPATH, "//span[contains(text(), 'Sortable')]")

    LIST_SORTABLE = (By.CSS_SELECTOR, "a#demo-tab-list")
    LIST_ITEMS = (By.CSS_SELECTOR, "div.vertical-list-container > div.list-group-item")

class SelectableInteractionsPageLocators:
    ##-------------------------------------------------------------------
    # Selectable - раздел на странице Interactions
    SELECTABLE = (By.XPATH, "//span[contains(text(), 'Selectable')]")

    LIST_SELECTABLE = (By.CSS_SELECTOR, "a#demo-tab-list")

    LIST_NO_SELECT = (By.CSS_SELECTOR, "li.mt-2.list-group-item")
    LIST_SELECT = (By.CSS_SELECTOR, "li.mt-2.list-group-item.active")

class DroppableInteractionsPageLocators:
    ##-------------------------------------------------------------------
    # Droppable - раздел на странице Interactions
    DROPPABLE = (By.XPATH, "//span[contains(text(), 'Droppable')]")

    PREVENT_PROPOGATION = (By.CSS_SELECTOR, "a#droppableExample-tab-preventPropogation")

    DRAG_BOX = (By.CSS_SELECTOR, "div#dragBox")
    DROP_BOX_INNER = (By.CSS_SELECTOR, "div#greedyDropBoxInner")

    DROP_BOX_INNER_COLOR = (By.CSS_SELECTOR, "div.drop-box.ui-droppable.ui-state-highlight")

class DragabbleInteractionsPageLocators:
    ##-------------------------------------------------------------------
    # Dragabble - раздел на странице Interactions
    DRAGABBLE = (By.XPATH, "//span[contains(text(), 'Dragabble')]")

    CONTAINER_RESTRICTION = (By.CSS_SELECTOR, "a#draggableExample-tab-containerRestriction")

    BIG_BLOC = (By.CSS_SELECTOR, "div#containmentWrapper")
    SMALL_BLOC = (By.CSS_SELECTOR, "div.draggable.ui-widget-content.ui-draggable.ui-draggable-handle")
