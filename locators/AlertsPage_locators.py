from selenium.webdriver.common.by import By

class AlertsAlertsPageLocators:
    # Alerts - раздел на странице Alerts, Frame & Windows
    ALERTS = (By.XPATH, "//span[contains(text(), 'Alerts')]")

    BUTTON_SEE_ALERT = (By.CSS_SELECTOR, "button#alertButton")
    BUTTON_TIMER_ALERT = (By.CSS_SELECTOR, "button#timerAlertButton")
    BUTTON_CONFIRM = (By.CSS_SELECTOR, "button#confirmButton")
    BUTTON_PROMPT = (By.CSS_SELECTOR, "button#promtButton")

    CONFIRM_RESULT = (By.CSS_SELECTOR, "span#confirmResult")
    PROMPT_RESULT = (By.CSS_SELECTOR, "span#promptResult")

class FramesAlertsPageLocators:
    # Frames - раздел на странице Alerts, Frame & Windows
    FRAMES = (By.XPATH, "//span[contains(text(), 'Frames')]")

    FRAME_MAX = (By.CSS_SELECTOR, "iframe#frame1")
    FRAME_MINI = (By.CSS_SELECTOR, "iframe#frame2")

    FRAME_TEXT = (By.CSS_SELECTOR, "h1#sampleHeading")
