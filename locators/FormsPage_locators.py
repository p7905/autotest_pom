from selenium.webdriver.common.by import By

class FormsPage_locators():
    # Practice Form - раздел на странице Forms
    PRACTICE_FORM = (By.XPATH, "//span[contains(text(), 'Practice Form')]")

    # поля формы
    FIRST_NAME = (By.CSS_SELECTOR, "input#firstName")
    LAST_NAME = (By.CSS_SELECTOR, "input#lastName")
    EMAIL = (By.CSS_SELECTOR, "input#userEmail")
    GENDER = (By.XPATH, "//label[contains(@for, 'gender-radio-')]")
    MOBILE = (By.CSS_SELECTOR, "input#userNumber")
    DATE_OF_BIRTH = (By.CSS_SELECTOR, "input#dateOfBirthInput")
    SUBJECTS = (By.CSS_SELECTOR, "input#subjectsInput")
    HOBBIES = (By.XPATH, "//label[contains(@for, 'hobbies-checkbox-')]")
    CURRENT_ADDRESS_FORMS = (By.CSS_SELECTOR, "textarea#currentAddress")

    SELECT_STATE = (By.CSS_SELECTOR, 'div#state')
    STATE_INPUT = (By.CSS_SELECTOR, 'input#react-select-3-input')
    STATE = (By.CSS_SELECTOR, 'div#state  div.css-1uccc91-singleValue')

    SELECT_CITY = (By.CSS_SELECTOR, 'div#city')
    CITY_INPUT = (By.CSS_SELECTOR, 'input#react-select-4-input')
    CITY = (By.CSS_SELECTOR, 'div#city  div.css-1uccc91-singleValue')

    SUBMIT_FORMS = (By.CSS_SELECTOR, "button#submit")

    VALUES = (By.XPATH, "//div[@class='table-responsive']//td[2]")
    KEYS = (By.XPATH, "//div[@class='table-responsive']//td[1]")

    MODAL_DIALOG = (By.CSS_SELECTOR, ".modal-dialog")

    INVALID_INPUT = (By.CSS_SELECTOR, ".form-control:invalid") # поля
    INVALID_RADIO = (By.CSS_SELECTOR, ".custom-control-input:invalid~.custom-control-label") # радио
