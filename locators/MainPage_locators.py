from selenium.webdriver.common.by import By

class MainPageLocators:
	CARD_ELEMENTS = (By.XPATH, "//h5[contains(text(), 'Elements')]/../../..")
	CARD_FORMS = (By.XPATH, "//h5[contains(text(), 'Forms')]/../../..")
	CARD_ALERTS = (By.XPATH, "//h5[contains(text(), 'Alerts')]/../../..")
	CARD_WIDGETS = (By.XPATH, "//h5[contains(text(), 'Widgets')]/../../..")
	CARD_INTERACTIONS = (By.XPATH, "//h5[contains(text(), 'Interactions')]/../../..")	
	CARD_BOOK = (By.XPATH, "//h5[contains(text(), 'Book')]/../../..")

	# шапка общая - может быть в BasePage???????///
	LOGO_MAIN_PAGE_LINK = (By.CSS_SELECTOR, "header a[href]")
	LOGO_MAIN_PAGE = (By.CSS_SELECTOR, "header img")