from selenium.webdriver.common.by import By

class TextBoxElPageLocators:
	TEXT_BOX = (By.XPATH, "//span[contains(text(), 'Text Box')]")

	# поля для ввода
	FULL_NAME = (By.CSS_SELECTOR, "#userName")
	EMAIL = (By.CSS_SELECTOR, "#userEmail")
	CUR_ADDRESS = (By.CSS_SELECTOR, "#currentAddress")
	PERM_ADDRESS = (By.CSS_SELECTOR, "#permanentAddress")

	#кнопка отправки введенных данных
	BUTTON_SUBMIT = (By.CSS_SELECTOR, "#submit")

	#блок с результатом
	OUTPUT_BLOCK = (By.CSS_SELECTOR, "#output")
	ELEMENTS_OUTPUT_BLOCK = (By.CSS_SELECTOR, "#output p")

	OUTPUT_FULL_NAME = (By.CSS_SELECTOR, "p#name")
	OUTPUT_EMAIL = (By.CSS_SELECTOR, "p#email")
	OUTPUT_CUR_ADDRESS = (By.CSS_SELECTOR, "p#currentAddress")
	OUTPUT_PERM_ADDRESS = (By.CSS_SELECTOR, "p#permanentAddress")

	# этот класс обозначает что в поле ошибка
	ERROR_ELEMENTS = (By.CSS_SELECTOR, ".field-error")

class CheckBoxElPageLocators:
	CHECK_BOX = (By.XPATH, "//span[contains(text(), 'Check Box')]")

	BUTTON_EXPAND_ALL = (By.CSS_SELECTOR, "button.rct-option-expand-all")

	ITEM_LIST = (By.CSS_SELECTOR, "input[type='checkbox']")

	CHECK_RESULT = (By.CSS_SELECTOR, "#result")
	LIST_CHECK_RESULT = (By.CSS_SELECTOR, "#result > .text-success")

class RadioButtonElPageLocators:
	RADIO_BUTTON = (By.XPATH, "//span[contains(text(), 'Radio Button')]")

	YES_BUTTON = (By.CSS_SELECTOR, "label[for='yesRadio']")
	YES_CHECK = (By.CSS_SELECTOR, "input#yesRadio")
	NO_BUTTON = (By.CSS_SELECTOR, "label[for='noRadio']")
	NO_CHECK = (By.CSS_SELECTOR, "input#noRadio")
	IMPRESSIVE_BUTTON = (By.CSS_SELECTOR, "label[for='impressiveRadio']")
	IMPRESSIVE_CHECK = (By.CSS_SELECTOR, "input#impressiveRadio")

	#блок с результатом
	RADIO_RESULT = (By.XPATH, "//p[contains(text(), 'You have selected')]")
	RADIO_CHECK_RESULT = (By.CSS_SELECTOR, "span.text-success")

class WebTablesElPageLocators:
	WEB_TABLES = (By.XPATH, "//span[contains(text(), 'Web Tables')]")

	ADD_ROW_BUTTON = (By.CSS_SELECTOR, "button#addNewRecordButton")
	USER_FORM = (By.CSS_SELECTOR, "form#userForm")

	# поля формы
	FIRST_NAME = (By.CSS_SELECTOR, "input#firstName")
	LAST_NAME = (By.CSS_SELECTOR, "input#lastName")
	USER_EMAIL = (By.CSS_SELECTOR, "input#userEmail")
	AGE = (By.CSS_SELECTOR, "input#age")
	SALARY = (By.CSS_SELECTOR, "input#salary")
	DEPARTMENT = (By.CSS_SELECTOR, "input#department")

	#кнопка отправки введенных данных
	ADD_USER_SUBMIT = (By.CSS_SELECTOR, "#submit")

	# строки
	ROW = (By.CSS_SELECTOR, "div.rt-tr-group")
	# кнопка редактирования в строке
	EDIT_BUTTON = (By.CSS_SELECTOR, "span[title='Edit']")
	# кнопка удаления в строке
	DELETE_BUTTON = (By.CSS_SELECTOR, "span[title='Delete']")

	# пагинация
	SELECT_PER_PAGE = (By.CSS_SELECTOR, "select[aria-label='rows per page']")
	NEXT_PAGE = (By.CSS_SELECTOR, "div.-next >button.-btn")

	PAGE_VALUE = (By.CSS_SELECTOR, "input[aria-label='jump to page']")

class ButtonsElPageLocators:
	BUTTONS = (By.XPATH, "//span[contains(text(), 'Buttons')]")

	DBL_CLICK_BUT = (By.CSS_SELECTOR, "button#doubleClickBtn")
	RGH_CLICK_BUT = (By.CSS_SELECTOR, "button#rightClickBtn")
	CLICK_BUT = (By.CSS_SELECTOR, "div.mt-4:nth-child(3) .btn")

	DBL_CLICK_MESSAGE = (By.CSS_SELECTOR, "p#doubleClickMessage")
	RGH_CLICK_MESSAGE = (By.CSS_SELECTOR, "p#rightClickMessage")
	CLICK_MESSAGE = (By.CSS_SELECTOR, "p#dynamicClickMessage")

class LinksElPageLocators:
	LINKS = (By.XPATH, "//span[contains(text(), 'Links')]")

	HOME_LINK = (By.CSS_SELECTOR, "a#simpleLink")

	CREATED_REQ = (By.CSS_SELECTOR, "a#created")
	NO_CONTENT_REQ = (By.CSS_SELECTOR, "a#no-content")
	MOVED_REQ = (By.CSS_SELECTOR, "a#moved")
	BAD_REQ = (By.CSS_SELECTOR, "a#bad-request")
	UNAUTHORIZED_REQ= (By.CSS_SELECTOR, "a#unauthorized")
	FORBIDDEN_REQ = (By.CSS_SELECTOR, "a#forbidden")
	NOT_FOUND_REQ = (By.CSS_SELECTOR, "a#invalid-url")

	RESPONSE_MESSAGE = (By.CSS_SELECTOR, "p#linkResponse > b")

class BrokenLinksElPageLocators:
	BROKEN_LINKS = (By.XPATH, "//span[contains(text(), 'Broken Links')]")

	IMAGE = (By.XPATH, "//p[contains(text(), 'Valid image')]/following-sibling::img")

	VALID_LINK = (By.XPATH, "//a[contains(text(), 'Valid Link')]")
	BROKEN_LINK = (By.XPATH, "//a[contains(text(), 'Broken Link')]")

class DynamicPropertiesElPageLocators:
	DYNAMIC_PROPERTIES = (By.XPATH, "//span[contains(text(), 'Dynamic Properties')]")

	BUTTON_ENABLED_AFTER = (By.CSS_SELECTOR, "button#enableAfter")
	BUTTON_COLOR_CHANGE = (By.CSS_SELECTOR, "button#colorChange")
	BUTTON_VISIBLE_AFTER = (By.CSS_SELECTOR, "button#visibleAfter")