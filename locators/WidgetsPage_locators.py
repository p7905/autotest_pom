from selenium.webdriver.common.by import By

class AccordianWidgetsPageLocators:
    # Accordian - раздел на странице Widgets
    ACCORDIAN = (By.XPATH, "//span[contains(text(), 'Accordian')]")

    ACCORDIAN_CARD1 = (By.CSS_SELECTOR, "div#section1Heading")
    ACCORDIAN_CARD2 = (By.CSS_SELECTOR, "div#section2Heading")
    ACCORDIAN_CARD3 = (By.CSS_SELECTOR, "div#section3Heading")

    ACCORDIAN_CARD1_TEXT = (By.CSS_SELECTOR, "div#section1Content > p")
    ACCORDIAN_CARD2_TEXT = (By.CSS_SELECTOR, "div#section2Content > p")
    ACCORDIAN_CARD3_TEXT = (By.CSS_SELECTOR, "div#section3Content > p")

class AutoCompliteWidgetsPageLocators:
    ##-------------------------------------------------------------------
    # Auto Complite - раздел на странице Widgets
    AUTO_COMPLIT = (By.XPATH, "//span[contains(text(), 'Auto Complete')]")

    AUTO_COMPLIT_INPUT_MULT = (By.CSS_SELECTOR, "input#autoCompleteMultipleInput")
    AUTO_COMPLIT_INPUT_SING = (By.CSS_SELECTOR, "input#autoCompleteSingleInput")

    AUTO_COMPLIT_MULT_TEXT= (By.CSS_SELECTOR, "div.css-1rhbuit-multiValue.auto-complete__multi-value")
    AUTO_COMPLIT_SING_TEXT= (By.CSS_SELECTOR, "div.auto-complete__single-value.css-1uccc91-singleValue")

    AUTO_COMPLIT_SELECT = (By.CSS_SELECTOR, "div.auto-complete__option")

class DatePickerWidgetsPageLocators:
    ##-------------------------------------------------------------------
    # Date Picker - раздел на странице Widgets
    DATE_PICKER = (By.XPATH, "//span[contains(text(), 'Date Picker')]")

    DATE_PICKER_BLOC = (By.CSS_SELECTOR, "div.react-datepicker")
    DATE_AND_TIME_INPUT = (By.CSS_SELECTOR, "input#dateAndTimePickerInput")

    YEAR_BLOC = (By.CSS_SELECTOR, "div.react-datepicker__year-read-view")
    # видимый список годов из 11 шт + 2 кнопки навигации
    YEAR_SELECT = (By.CSS_SELECTOR, "div.react-datepicker__year-option")
    # выбранный год
    YEAR_SELECTED = (By.CSS_SELECTOR, "div.react-datepicker__year-option--selected_year")
    # навигация по списку
    YEAR_NAVIGATION_PREV = (By.CSS_SELECTOR, "a.react-datepicker__navigation--years-previous")
    YEAR_NAVIGATION_UPCOM = (By.CSS_SELECTOR, "a.react-datepicker__navigation--years-upcoming")

    MONTH_BLOC = (By.CSS_SELECTOR, "div.react-datepicker__month-read-view")
    MONTH_SELECT = (By.CSS_SELECTOR, "div.react-datepicker__month-option")
    # выбранный месяц
    MONTH_SELECTED = (By.CSS_SELECTOR, "div.react-datepicker__month-option--selected_month")

    DATE_SELECT = (By.CSS_SELECTOR, "div.react-datepicker__day:not(.react-datepicker__day--outside-month)")
    TIME_SELECT = (By.CSS_SELECTOR, "li.react-datepicker__time-list-item")

class SliderWidgetsPageLocators:
    ##-------------------------------------------------------------------
    # Slider - раздел на странице Widgets
    SLIDER = (By.XPATH, "//span[contains(text(), 'Slider')]")

    SLIDER_INPUT = (By.CSS_SELECTOR, "input.range-slider")
    SLIDER_VALUE = (By.CSS_SELECTOR, "input#sliderValue")

class ProgressBarWidgetsPageLocators:
    ##-------------------------------------------------------------------
    # Progress bar - раздел на странице Widgets
    PROGRESS_BAR = (By.XPATH, "//span[contains(text(), 'Progress Bar')]")

    BUTTON_START_STOP = (By.CSS_SELECTOR, "button#startStopButton")
    BUTTON_RESET = (By.CSS_SELECTOR, "button#resetButton")

    PROGRESS_INFO = (By.CSS_SELECTOR, "div.progress-bar.bg-info")
    PROGRESS_SUCCESS = (By.CSS_SELECTOR, "div.progress-bar.bg-success")

class TabsWidgetsPageLocators:
    ##-------------------------------------------------------------------
    # Tabs - раздел на странице Widgets
    TABS = (By.XPATH, "//span[contains(text(), 'Tabs')]")

    NAV_TABS_LIST = (By.CSS_SELECTOR, "a.nav-item.nav-link:not(.disabled)")

    WHAT_TEXT = (By.CSS_SELECTOR, "div#demo-tabpane-what > p")
    ORIGIN_TEXT = (By.CSS_SELECTOR, "div#demo-tabpane-origin > p")
    USE_TEXT = (By.CSS_SELECTOR, "div#demo-tabpane-use > p")

class ToolTipsWidgetsPageLocators:
    ##-------------------------------------------------------------------
    # Tool Tips - раздел на странице Widgets
    TOOL_TIPS = (By.XPATH, "//span[contains(text(), 'Tool Tips')]")

    TEXT_LINK = (By.XPATH, "//a[contains(text(), 'Contrary')]")
    TOOLTIP = (By.CSS_SELECTOR, "div.tooltip-inner")

class MenuWidgetsPageLocators:
    ##-------------------------------------------------------------------
    # Menu - раздел на странице Widgets
    MENU = (By.XPATH, "//span[contains(text(), 'Menu')]")

    MAIN_ITEM_2 = (By.XPATH, "//li/a[contains(text(), 'Main Item 2')]")
    SUB_SUB_LIST = (By.XPATH, "//li/a[contains(text(), 'SUB SUB LIST »')]")
    SUB_SUB_ITEM_2 = (By.XPATH, "//li/a[contains(text(), 'Sub Sub Item 2')]")