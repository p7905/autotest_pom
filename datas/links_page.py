
class LinksPage:
	LINK_MAIN_PAGE = "https://demoqa.com/"
	LINK_ELEMENTS = "https://demoqa.com/elements"
	LINK_FORMS = "https://demoqa.com/forms"
	LINK_ALERTS = "https://demoqa.com/alertsWindows"
	LINK_WIDGETS = "https://demoqa.com/widgets"
	LINK_INTERACTIONS = "https://demoqa.com/interaction"

