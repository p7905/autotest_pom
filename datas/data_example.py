import datetime

class CheckBox:
    list_item_check_box = ['Notes', 'Angular', 'General', 'Word File.doc']


class Forms:
    date = datetime.date(1980, 2, 23)
    name_subject = ["Math", "Physics", "Arts", "Biology", "Chemistry"]
    data_forms = [
        ['', None, None, None, None],
        [None, '', None, None, None],
        [None, None, 'test@', None, None],
        [None, None, None, -1, None],
        [None, None, None, None, '12345']]

class AutoComplite:
    color_full_sing = ['blue', 'indigo']
    color_full_mult = ['red', 'purple', 'aqua']

class DatePicker:
    date_time = datetime.datetime(1940, 11, 27, 12, 00)

class Slider:
    value_slider = 72

class Sortable:
    list_sort = ['Six', 'Four', 'Two', 'Five', 'Three', 'One']

class Frame:
    size = '1000'

