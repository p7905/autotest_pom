from methods.BasePage import BasePage

class MainPage(BasePage):

    def __init__(self, *args, **kwargs):
        super(MainPage, self).__init__(*args, **kwargs)

    def get_attribute_for_element(self, how, what, name_attr):
        element = self.browser.find_element(how, what)
        value_attr = element.get_attribute(name_attr) # ссылка должна быть указана в атрибуте href

        return value_attr

    def get_url_after_go_to_on_card(self, how, what):
        card = self.browser.find_element(how, what,)
        self.go_to_element(card)
        card.click()

        return self.browser.current_url