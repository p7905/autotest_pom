from methods.BasePage import BasePage

import random
##import time
##
##from selenium.webdriver.support.ui import Select
##from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

class FormsPage(BasePage):

    def __init__(self, *args, **kwargs):
        super(FormsPage, self).__init__(*args, **kwargs)

##    def remove_footer(self):
##        # убираем элементы футера, которые перекрывают доступ
##        self.browser.execute_script("document.getElementById('close-fixedban').remove();")
##        self.browser.execute_script("document.getElementsByTagName('footer')[0].remove();")

    def fill_name(self, how, what, name=None):
        if name is None:
            name = f'Test Name {random.randint(1,10)}'

        self.browser.find_element(how, what).send_keys(name)

        return name

    def fill_email(self, how, what, email=None):
        if email is None:
            email = f'TestEmail{random.randint(1,10)}@test.com'

        self.browser.find_element(how, what).send_keys(email)

        return email

    def fill_phone(self, how, what, phone=None):
        if phone is None:
            first = [7, 8][random.randint(0, 1)]

            # Последние 7 цифр
            suffix = random.randint(9999999, 100000000)

            # Сшиваем номер мобильного телефона
            phone = "{}9{}".format(first, suffix)

        self.browser.find_element(how, what).send_keys(phone)

        return phone

    def fill_address(self, how, what):
        address = f'Test Address, 25 / 5'

        element = self.browser.find_element(how, what)
        self.go_to_element(element)
        element.send_keys(address)

        return address

    def fill_date_of_birth(self, how, what, date_value):
        date_of_birth = self.browser.find_element(how, what)
        date_of_birth.click()

        date_of_birth.send_keys(Keys.CONTROL, "a")
        date_of_birth.send_keys(f"{date_value}")
        date_of_birth.send_keys(Keys.ENTER)

        return date_value

    def fill_subjects(self, how, what, list_subject):
        subjects = self.browser.find_element(how, what)

        name_subject = random.choice(list_subject)
        subjects.send_keys(name_subject)
        subjects.send_keys(Keys.RETURN)

        return name_subject

    def fill_gender(self, how, what, gender=None):
        if gender == None:
            select_gender = random.randint(0,2)

            gender_list = self.browser.find_elements(how, what)
            gender_list[select_gender].click()

            return gender_list[select_gender].text

        return ''

    def fill_hobbies(self, how, what):
        select_hobbies = random.randint(1, 3)
        select_res = list()

        hobbies_list = self.browser.find_elements(how, what)
##        self.go_to_element(hobbies_list)


        for k in range(0, select_hobbies):
            hobbies_list[k].click()
            select_res.append(hobbies_list[k].text)

        return select_res

    def fill_state_or_city(self, *args_loc):
        element = self.browser.find_element(args_loc[0], args_loc[1])
        self.go_to_element(element)
        element.click()

        self.browser.find_element(args_loc[2], args_loc[3]).send_keys(Keys.RETURN)

        return self.browser.find_element(args_loc[4], args_loc[5]).text

    def data_in_modal(self, how_key, what_key, how_val, what_val):

        keys = [item.text for item in self.browser.find_elements(how_key, what_key)]
        values = [item.text for item in self.browser.find_elements(how_val, what_val)]

        return dict(zip(keys, values))

    @staticmethod
    def compar_data(data_fill, data_res):

        if data_res['Student Name'] != data_fill['First Name'] + ' ' + data_fill['Last Name']:
            return False, 'Student Name'
        if data_res['Student Email'] != data_fill['Email']:
            return False, 'Student Email'
        if data_res['Gender'] != data_fill['Gender']:
            return False, 'Gender'
        if data_res['Mobile'] != data_fill['Mobile']:
            return False, 'Mobile'
        if data_res['Date of Birth'] != data_fill['Date of Birth'].strftime("%d %B,%Y"):
            return False, 'Date of Birth'
        if len(data_fill['Hobbies']) > 0:
            if data_res['Hobbies'].split(', ') != data_fill['Hobbies']:
                return False, 'Hobbies'
        else:
            if len(data_res['Hobbies']) != 0:
                return False, 'Hobbies'
        if data_res['Picture'] != data_fill['Picture']:
            return False, 'Picture'
        if data_res['State and City'] != f"{data_fill['State']} {data_fill['Сity']}".strip():
            return False, 'State and City'

        return True, ""

    def element_is_novalid_data_fill(self, how_in, what_in, how_r, what_r):
        element = self.browser.find_element(how, what)

        element_inv = self.browser.find_elements(how_in, what_in)
        element_inv_r = self.browser.find_elements(how_r, what_r)

        if element in element_inv or element in element_inv_r:
            return True

        return False

    def elements_are_novalid_data_fill(self, how_in, what_in, how_r, what_r):

        element_inv = self.browser.find_elements(how_in, what_in)
        element_inv_r = self.browser.find_elements(how_r, what_r)

        if len(element_inv) == 0 and len(element_inv_r) == 0:
            return True

        return False

    def click_element_execute_script(self, how, what):
        self.browser.execute_script ("arguments[0].click();", self.browser.find_element(how, what))