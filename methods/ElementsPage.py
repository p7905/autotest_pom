from methods.BasePage import BasePage

import random
import time

from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

class ElementsPage(BasePage):

    def __init__(self, *args, **kwargs):
        super(ElementsPage, self).__init__(*args, **kwargs)

    def submit_data(self, how, what):
        button = self.browser.find_element(how, what)
        # на кнопку можно нажать, если она в поле зрения
        self.go_to_element(button)
        button.click()

    # True  - заполняем валидными,
    # False - заплняем невалидными,
    # None  - не заполняем
    def generate_random_data(self, fname=True, email=True, caddress=True, paddress=True):
        input_data = {  'Full Name': "",
                        'Email': "",
                        'Current Address': "",
                        'Permanent Address': ""
                    }

        count_fill_fields = 0

        if fname:
            input_data['Full Name'] = f'Test Full Name {random.randint(1,10)}'
            count_fill_fields += 1

        if email:
            input_data['Email'] = f'TestEmail{random.randint(1,10)}@test.com'
            count_fill_fields += 1
        else:
            input_data['Email'] = f'TestEmail{random.randint(1,10)}'
            count_fill_fields += 1

        if caddress:
            input_data['Current Address'] = f'Test Current Address {random.randint(1,10)} / {random.randint(20,30)}'
            count_fill_fields += 1

        if paddress:
            input_data['Permanent Address'] = f'Test Permanent Address, {random.randint(1,10)} , {random.randint(20,30)}'
            count_fill_fields += 1

        return input_data, count_fill_fields

    def edit_data_in_table(self, how, what, value):
        element = self.browser.find_element(how, what)
        element.clear()
        element.send_keys(value)

    def checking_radio_button(self, how_but, what_but, how_check, what_check):
        radio_button = self.browser.find_element(how_but, what_but)
        radio_button.click()
        radio_button_ch = self.browser.find_element(how_check, what_check)

        return radio_button, radio_button_ch

    @staticmethod
    def generate_random_data_table():
        input_data_table = dict()

        input_data_table['First Name'] = f'Test Full Name {random.randint(1,10)}'
        input_data_table['Last Name'] = f'Test Last Name {random.randint(10,20)}'
        input_data_table['Age'] = f'{random.randint(18,100)}'
        input_data_table['User Email'] = f'TestUserEmail{random.randint(1,10)}@test.com'
        input_data_table['Salary'] = f'{random.randint(50000,120000)}'
        input_data_table['Department'] = f'Test Department {random.randint(1,10)}'

        return input_data_table

    def clicks_element(self, type_click, how, what):
        actions = ActionChains(self.browser)
        if type_click == "double": actions.double_click(self.browser.find_element(how, what))
        if type_click == "right": actions.context_click(self.browser.find_element(how, what))
        if type_click == "click": actions.click(self.browser.find_element(how, what))
        actions.perform()

    def get_naturalWidth_img(self, element):
        return self.browser.execute_script("return arguments[0].naturalWidth;", element)


    def get_url_page(self, num):
        self.browser.switch_to.window(self.browser.window_handles[num])

        return self.browser.current_url

    def go_to_page(self, how, what, num_page):
        page_value = self.browser.find_element(how, what)
        page_value.click()
        page_value.send_keys(Keys.DELETE)
        page_value.send_keys(f"{num_page}\n")
        time.sleep(5)

    def select_per_page(self, how, what, cnt_per_page):
        select_page = Select(self.browser.find_element(how, what))
        select_page.select_by_value(f"{cnt_per_page}")