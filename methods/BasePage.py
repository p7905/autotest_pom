from selenium.common import exceptions as E
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains

class BasePage:
    def __init__(self, browser, url):
        self.browser = browser
        self.url = url

    def open(self, new_url=""):
        if new_url == "":
            self.browser.get(self.url)
        else:
            self.browser.get(new_url)

    def element_is_present(self, how, what, timeout=5):
        try:
            WebDriverWait(self.browser, timeout).until(EC.presence_of_element_located((how, what)))
        except E.TimeoutException:
            return False
        return True

    def element_is_visible(self, how, what, timeout=5):
        try:
            WebDriverWait(self.browser, timeout).until(EC.visibility_of_element_located((how, what)))
        except E.TimeoutException:
            return False
        return True

    def element_is_not_visible(self, how, what, timeout=5):
        try:
            WebDriverWait(self.browser, timeout).until(EC.invisibility_of_element_located((how, what)))
        except E.TimeoutException:
            return False
        return True

    def element_is_include_attribute(self, how, what, attribut, timeout=5):
        try:
            WebDriverWait(self.browser, timeout).until(EC.element_attribute_to_include((how,what), attribut))
        except E.TimeoutException:
            return False
        return True

    def go_to_element(self, element):
        self.browser.execute_script("arguments[0].scrollIntoView();", element)

    def go_to_and_click(self, how, what):
        element = self.browser.find_element(how, what)

        self.go_to_element(element)
        element.click()

    def alert_is_present(self, timeout=6):

        try:
            WebDriverWait(self.browser, timeout).until(EC.alert_is_present())
        except E.TimeoutException:
            return False

        return True

    def set_attribut(self, element, name_attr, value_attr):
        self.browser.execute_script(f"arguments[0].setAttribute('{name_attr}', '{value_attr}')", element)

    def element_is_clickable(self, how, what, timeout=5):
        try:
            WebDriverWait(self.browser, timeout).until(EC.element_to_be_clickable((how, what)))
        except E.TimeoutException:
            return False
        return True

    def move_cursor_to_element(self, element):
        action = ActionChains(self.browser)
        action.move_to_element(element)
        action.perform()

    def drag_and_drop_element(self, element_drag, element_drop):
        action = ActionChains(self.browser)
        action.drag_and_drop(element_drag, element_drop)
        action.perform()

    def drag_and_drop_by_offset(self, element_drag, x=0, y=0):
        action = ActionChains(self.browser)
        action.drag_and_drop_by_offset(element_drag, x, y)
        action.perform()