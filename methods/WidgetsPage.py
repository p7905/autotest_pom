from methods.BasePage import BasePage

from selenium.webdriver.common.action_chains import ActionChains

class WidgetsPage(BasePage):

    def __init__(self, *args, **kwargs):
        super(WidgetsPage, self).__init__(*args, **kwargs)

    def select_full_color_from_part_name(self, element_auto_complit, color_full, how, what):
        element_auto_complit.click()
        element_auto_complit.send_keys(color_full[0])

        if self.element_is_visible(how, what):
            select_list = self.browser.find_elements(how, what)
            for color in select_list:
                if color.text.upper() == color_full.upper():
                    color.click()
                    break

    def select_month(self, month_select, date_input, MONTH_SELECTED, MONTH_SELECT):
        month_now = date_input.replace(',', '').split(' ')[0]

        if month_now == month_select:
            self.browser.find_element(*MONTH_SELECTED).click()
        else:
            select_elements = self.browser.find_elements(*MONTH_SELECT)
            for elem in select_elements:
                if elem.text == str(month_select):
                    elem.click()
                    break

    def select_year(self, year_select, date_input, YEAR_SELECTED, YEAR_SELECT, YEAR_NAVIGATION_PREV):
        count_year = 5
        year_now = int(date_input.replace(',', '').split(' ')[2])

        if year_now == year_select:
            self.browser.find_element(*YEAR_SELECTED).click()
        elif year_now - count_year <= year_select <= year_now - count_year:
            select_elements = self.browser.find_elements(*YEAR_SELECT)
            for elem in select_elements:
                if elem.text == str(year_select):
                    elem.click()
                    break
        elif year_now - count_year >= year_select:
            # кликаем вниз и смотрим 1 элемент
            while True:
                self.browser.find_element(*YEAR_NAVIGATION_PREV).click()
                select_elements = self.browser.find_elements(*YEAR_SELECT)
                last_elem = len(select_elements) - 2
                if select_elements[last_elem].text == str(year_select):
                    select_elements[last_elem].click()
                    break
        else:
            # кликаем вверх и смотрим 1 элемент
            while True:
                self.browser.find_element(*YEAR_NAVIGATION_UPCOM).click()
                select_elements = self.browser.find_elements(*YEAR_SELECT)
                if select_elements[1].text == str(year_select):
                    select_elements[1].click()
                    break

    def select_date(self, date_select, date_input, DATE_SELECT):
        select_elements = self.browser.find_elements(*DATE_SELECT)

        for elem in select_elements:
            if elem.text == str(date_select):
                elem.click()
                break

    def select_time(self, time_select, date_input, TIME_SELECT):
        select_elements = self.browser.find_elements(*TIME_SELECT)

        for elem in select_elements:
            if str(elem.text) == str(time_select):
                elem.click()
                break

    def select_value_on_slider(self, element, xoffset=0, yoffset=0):
        action = ActionChains(self.browser)
        action.drag_and_drop_by_offset(element, xoffset, yoffset)
        action.perform()