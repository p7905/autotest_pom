from methods.BasePage import BasePage

class InteractionsPage(BasePage):
    def __init__(self, *args, **kwargs):
        super(InteractionsPage, self).__init__(*args, **kwargs)

    @staticmethod
    def get_index_list(list_items, text):
        for i in range(len(list_items)):
            if list_items[i].text == text:
                return i

        return -1